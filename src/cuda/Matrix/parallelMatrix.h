//
// Created by benjamin angelard on 28/10/2017.
//

#ifndef CUDA_PARALLELMATRIX_H
#define CUDA_PARALLELMATRIX_H
#include "macros.h"
#include "macros.hpp"
#include "CPU_Matrix.h"

/* Test Function */
void test_Generation_GPU_integerImage();
/* GPU */
/** 
 ** La fonction kernel appelé pour calculer les images intégrale
 ** IntegerImage le tableau qui recuperea toutes les images intégrales sous formes de lignes 
 **/ 
__global__ void Compute_List_Integral_gpu(double*  vector_picture, int * nb_image, int  * size_image, double *IntegerImage);



/** Fonction utilitaire pour afficher une image depuis la GPU, Debug **/ 
__device__ void print_image(double *iiImage, int size);
/* CPU to GPU */
/** 
 ** Fonction appelé pour calculer les images intégrale des nb Images 
 ** Nb image le nombre d'image à traiter 
 ** size_image la taille d'une image en ligne
 ** Vect image le tableau des images 
 **/
double * sendPicturesGPU(int nb_image, int size_image, double * vect_image);
/* CPU */
/**
** Fonction utilitaire pour afficher une image depuis la CPU , fonction de Debug 
**/ 
__host__ void print_image_cpu(double *iiImage, int size);



#endif //CUDA_PARALLELMATRIX_H
