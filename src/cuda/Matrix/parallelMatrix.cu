#include <iostream>
#include "parallelMatrix.h"
#include "cuda_runtime.h"
#include "device_launch_parameters.h"
#include <cooperative_groups.h>
#define BENCHMARK true

#include<time.h>



/* GPU FUNCTION */


__device__ void print_image(double *iiImage, int size)
{
    for (int i = 0; i < size * size; i++)
    {
        printf(" %f - ", iiImage[i]);
        if ((i + 1) % size == 0)
        {
            printf("\n");
        }
    }
}

__host__ void print_image_cpu(double *iiImage, int size)
{
    for (int i = 0; i < size * size; i++)
    {
        printf(" %f - ", iiImage[i]);
        if ((i + 1) % size == 0)
        {
            printf("\n");
        }
    }
}




using namespace cooperative_groups;
__global__ void Compute_List_Integral_gpu(double*  vector_picture, int *nb_image, int *size_image, double *IntegerImage)
{

    int index = threadIdx.x + blockIdx.x * blockDim.x;
    /* Debordement des besoins pour copier l'image*/
    if (index >= *size_image *  *size_image * *size_image * *nb_image) 
        return;

     if (index <  *size_image * *size_image * *nb_image) /* Debordement  des besoins pour compute taille*/
        {
            IntegerImage[index] = vector_picture[index];
     }   

     /* Calcule de l'image à traiter par le thread courant */ 
    int numero_image = index / (*size_image * *size_image); 
     /* Taille complete d'une image, size_image n'est que la taille d'une ligne */ 
    int size_complete =  *size_image * *size_image;
/* L'indexe de la colonne courante au thread */ 
    int pos_col = index % (*size_image * *size_image); 
    if (index % (*size_image * *size_image) < *size_image) /* Nombre de case d'une ligne , on ajoute la ligne juste au dessus en ignorant la premiere ligne*/
    {
        for (uint32_t i = 1; i < *size_image ; i++) /* Parcours les lignes une a une en ignorant la premiere */
            {
            int num_ligne = i ;
             uint32_t current_pos = numero_image * size_complete + num_ligne * *size_image  + pos_col; 
             IntegerImage[current_pos] +=  IntegerImage[current_pos - *size_image];   
             __syncthreads();
            
            }
    }
/* Additionne colonne du dessus */ 
    __syncthreads();
    uint32_t pos_line = *size_image * index;
    if (index  % *size_image == 0) /* Nombre de case d'une colonne */
    {
        for (int i = 1; i < *size_image ; i++) /* Nombre de colonne */
       {
            IntegerImage[index + i] = IntegerImage[index + i] + IntegerImage[index + i - 1];
            __syncthreads();
          g.sync();  
        }
    }
    __syncthreads();
        g.sync();

}





/* END FUNCTION GPU */


/* FUNCTION CPU TO GPU */

/*
** Comme son nom l'indique, prend en argument un tableau de double et le nombre d'element et copie en mémoire cuda le tableau après l'avoir allouer
** HostArray est le tableau a copier
** NumElement est le nombre d'élément du tableau
** Renvoie le pointeur Cuda pointant vers notre tableau en GPU
** Assure la gestion d'erreur du malloc et du Memcopy
*/
double *CopyArrayToGPU(double *HostArray, int NumElements)
{
    /* Calcul la taille en byte des data a envoyer */ 
    int bytes = sizeof(double) * NumElements;
    void *DeviceArray;
    std::cout << "Send " << bytes << " bytes " <<std::endl;
    // Allocate memory on the GPU for array
    auto error_code = cudaMalloc(&DeviceArray, bytes);
    if (error_code != cudaSuccess)
    {
        LOG(std::cerr << "[CopyArrayToGPU] CopyArrayToGPU(): Couldn't allocate mem for array on GPU." << std::endl)
        return NULL;
    }

    // Copy the contents of the host array to the GPU
    if (cudaMemcpy(DeviceArray, HostArray, bytes, cudaMemcpyHostToDevice) != cudaSuccess)
    {
        LOG(std::cerr << "CopyArrayToGPU(): Couldn't copy host array to GPU." << std::endl)
        cudaFree(DeviceArray);
        return NULL;
    }
    return (double *)DeviceArray;
}



__host__ double * sendPicturesGPU(int nb_image, int size_image, double * vect_image)
{
    double * Pointer_d_image = new double[nb_image * (size_image * size_image)];

    /* Le but ici est d'envoyer les images par paquet pour que tous les threads d'un bloc agissent au maximum */
    size_t * limit = (size_t *) malloc(sizeof(size_t)); /* Contient le nombre de thread maximum*/
    cudaDeviceGetLimit(limit, cudaLimitStackSize);
    int nb_t_Image = *limit / ((size_image) * size_image); /* Nombre d'image integrale a compute */
    int nb_bloc_required = nb_image / nb_t_Image + 1; /* Nombre de bloc a instancier pour nos traitements, 100 images de taille 10 => 10 blocs */ 

    /* Volontairement int pour cast inferieur */
    LOG(std::cerr << "[SendPictureGPU] Complete size " << size_image * nb_image << std::endl)
    LOG(std::cerr << "[SendPictureGPU] Max size of GPU thread " << *limit << " will handle " << nb_t_Image << "images " << std::endl)
    LOG(std::cerr << "[SendPictureGPU] Nb required block " << nb_bloc_required << std::endl)
    LOG(std::cerr << "[SendPictureGPU] Create an array of " << size_image * nb_image * size_image << std::endl)
    LOG(std::cerr << "[SendPictureGPU] Nb Image " << nb_image << std::endl)
   
    /* Envoie des données communes */
    int * d_size;
    cudaMalloc((void **)&d_size, sizeof(int));
    cudaMemcpy(d_size, &size_image, sizeof(int), cudaMemcpyHostToDevice); /* Transfer size to GPU */

    int * d_nb_image;
    cudaMalloc((void **)&d_nb_image, sizeof(int));
    cudaMemcpy(d_nb_image, &nb_image, sizeof(int), cudaMemcpyHostToDevice); /* Transfer size to GPU */

    double * d_integerImages = CopyArrayToGPU(vect_image, nb_image * ((size_image) * (size_image)));
    /* Combien de threads sont nécessaires pour handle les images */
    if (nb_t_Image * (size_image *  size_image) <= 64) { nb_t_Image = 64; }
    else if (nb_t_Image * (size_image *  size_image)<= 128) { nb_t_Image = 128;}
    else if(nb_t_Image * (size_image *  size_image)<= 256) { nb_t_Image = 256; }
    else if (nb_t_Image* (size_image *  size_image) <= 512 && *limit > 512) { nb_t_Image = 512; }
    else { nb_t_Image = 1024; }
	LOG(std::cerr << "[SendImageGPU] Nb thread utilise " << nb_t_Image << std::endl)
    auto d_Inputs = CopyArrayToGPU(vect_image, size_image * size_image  * nb_image) ;
    /* Une copie de notre image sur gpu, recupere un pointeur GPU */

      
	Compute_List_Integral_gpu<<<nb_bloc_required, nb_t_Image>>>(d_Inputs, d_nb_image, d_size, d_integerImages);
    

    /* Recuperation de l'integer image  */
    int  res_error = cudaMemcpy(Pointer_d_image, d_integerImages,nb_image * size_image * size_image * sizeof(double), cudaMemcpyDeviceToHost);
    return Pointer_d_image; 
}
/* END FUNCTION CPU TO GPU */
/* FUNCTION CPU */

