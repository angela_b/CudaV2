#ifndef __IMAGE_HPP__
#define __IMAGE_HPP__

#include <string>
#include <iostream>
#include <fstream>
#include <vector>

#include "macros.hpp"
#include "matrix.hpp"

// Get all raw images urls from a file
void geturls(const std::string images_urls_file, std::vector<std::string> &urls);

// Read a file containing all samples URLS, and load images one by one from those URLS
void readimages(const std::string images_urls_file, std::vector<Matrix<iicoef_t>*>& images, uint32_t limit);
void readimages(const std::vector<std::string>& images_urls, std::vector<Matrix<iicoef_t>*> &images, uint32_t begin, uint32_t end);

// Read a raw image (see scripts/to_raw image.py script), and load it in a matrix
void readimage(const std::string path, Matrix<iicoef_t>& image);

// Get the integral image from an image
void get_integralimage(const Matrix<iicoef_t>& image, Matrix<iicoef_t>& integralimage);

// Using an integral image, compute the sum of the pixels of the area delimited by (i1, j1) and (i2, j2)
// Exemple :
//						0	0	0	0
//		1	1	1		0	1	2	3			sum in (0, 0) -> (1, 1) : 4 - 0 - 0 + 0 = 4
//		1	1	1		0	2	4	6			sum in (0, 1) -> (2, 2) : 9 - 0 - 3 + 0 = 6
//		1	1	1		0	3	6	9
//		  Image 	    Integral Image
//
iicoef_t compute_pixels_sum(const Matrix<iicoef_t>& integralimage, uint32_t i1, uint32_t j1, uint32_t i2, uint32_t j2);


// Haar features computation

//      XXXXXXOOOOOO      XXXXXXXXXXXX      XXXXOOOOXXXX      XXXXXXXXXXXX      XXXXXXOOOOOO
//      XXXXXXOOOOOO      XXXXXXXXXXXX      XXXXOOOOXXXX      XXXXXXXXXXXX      XXXXXXOOOOOO
//      XXXXXXOOOOOO      XXXXXXXXXXXX      XXXXOOOOXXXX      OOOOOOOOOOOO      XXXXXXOOOOOO
//      XXXXXXOOOOOO      OOOOOOOOOOOO      XXXXOOOOXXXX      OOOOOOOOOOOO      OOOOOOXXXXXX
//      XXXXXXOOOOOO      OOOOOOOOOOOO      XXXXOOOOXXXX      XXXXXXXXXXXX      OOOOOOXXXXXX
//      XXXXXXOOOOOO      OOOOOOOOOOOO      XXXXOOOOXXXX      XXXXXXXXXXXX      OOOOOOXXXXXX
//         type 1            type 2            type 3            type 4            type 5


void compute_features_parameters(uint32_t image_height, uint32_t image_width, std::vector<std::vector<fparam_t>>& parameters);

void compute_features_values(	uint32_t image_height, uint32_t image_width, 
								const Matrix<iicoef_t> *integralimages, uint32_t ii_index, 
								Matrix<fvalue_t> &features_values);
#endif /* __IMAGE_HPP__ */
