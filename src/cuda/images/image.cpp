#include "image.hpp"


void geturls(const std::string images_urls_file, std::vector<std::string> &urls) 
{
	std::string line;
	std::ifstream file(images_urls_file);

	getline(file, line);
	uint32_t num_images = std::stoi(line);
	
	while (getline(file, line))
		urls.push_back(line);

	file.close();
}


// Read a file containing all samples URLS, and load images one by one from those URLS
void readimages(const std::string images_urls_file, std::vector<Matrix<iicoef_t>*> &images, uint32_t limit) 
{
	std::string line;
	std::ifstream file(images_urls_file);

	getline(file, line);
	uint32_t num_images = std::stoi(line);

	uint32_t k = 0;
	
	while (getline(file, line)) 
	{
		Matrix<iicoef_t> *image = new Matrix<iicoef_t>();
		readimage(line, *image);
		images.push_back(image);
		
		k += 1;
		if (k >= limit)
			break;
	}

	file.close();
}


void readimages(const std::vector<std::string>& images_urls, std::vector<Matrix<iicoef_t>*> &images, uint32_t begin, uint32_t end)
{
	for (uint32_t i = begin; i < end; i++)
	{
		Matrix<iicoef_t>* image = new Matrix<iicoef_t>();
		readimage(images_urls[i], *image);
		images.push_back(image);
	}
} 



// Read a raw image (see scripts/to_raw image.py script), and load it in a matrix
void readimage(const std::string path, Matrix<iicoef_t> &image) 
{
	std::string line;
	std::ifstream file(path);

	getline(file, line);
	uint32_t image_width = std::stoi(line);
	getline(file, line);
	uint32_t image_height = std::stoi(line);

	#ifdef CHECK_EXCEPTIONS
	if (image_height != SAMPLE_HEIGHT || image_width != SAMPLE_WIDTH) { throw std::length_error("readimage(const std::string path, Matrix &image: sample has invalid dimensions"); }
	#endif

	image.clear();
	image.init(image_height, image_width);

	for (uint32_t i = 0; i < image_height; i++) 
	{
		for (uint32_t j = 0; j < image_width; j++) 
		{
			getline(file, line);
			image.set((iicoef_t)stoi(line), i, j);
		}
	}

	file.close();
}

// Get the integral image from an image
void get_integralimage(const Matrix<iicoef_t> &image, Matrix<iicoef_t> &integralimage) 
{
	integralimage.init(image._h, image._w);

	// Copy the pixels values of the image
	for (uint32_t i = 0; i < image._h; i++) 
		for (uint32_t j = 0; j < image._w; j++) 
			integralimage.set(image.at(i, j), i, j);

	// Sum the rows one by one
	for (uint32_t i = 1; i < image._h; i++) 
		for (uint32_t j = 0; j < image._w; j++) 
			integralimage.set(integralimage.at(i - 1, j) + integralimage.at(i, j), i, j);

	// Sum the columns one by one
	for (uint32_t i = 0; i < image._h; i++)
		for (uint32_t j = 1; j < image._w; j++)
			integralimage.set(integralimage.at(i, j - 1) + integralimage.at(i, j), i, j);
}

// Using an integral image, compute the sum of the pixels of the area delimited by (i1, j1) and (i2, j2)
// Exemple :
//						0	0	0	0
//		1	1	1		0	1	2	3			sum in (0, 0) -> (1, 1) : 4 - 0 - 0 + 0 = 4
//		1	1	1		0	2	4	6			sum in (0, 1) -> (2, 2) : 9 - 0 - 3 + 0 = 6
//		1	1	1		0	3	6	9
//		  Image 	    Integral Image
//
iicoef_t compute_pixels_sum(const Matrix<iicoef_t> &integralimage, uint32_t i1, uint32_t j1, uint32_t i2, uint32_t j2) 
{	
	#ifdef CHECK_EXCEPTIONS
	if (i2 < i1 || j2 < j1) { throw std::out_of_range("compute_pixels_sum() : index out of range"); }
	#endif

	iicoef_t D = integralimage.at(i2, j2);
	iicoef_t A = (i1 == 0 || j1 == 0) ? 0 : integralimage.at(i1 - 1, j1 - 1);
	iicoef_t B = (i1 == 0) ? 0 : integralimage.at(i1 - 1, j2);
	iicoef_t C = (j1 == 0) ? 0 : integralimage.at(i2, j1 - 1);

	return D - B - C + A;
}


// Haar features computation

//      XXXXXXOOOOOO      XXXXXXXXXXXX      XXXXOOOOXXXX      XXXXXXXXXXXX      XXXXXXOOOOOO
//      XXXXXXOOOOOO      XXXXXXXXXXXX      XXXXOOOOXXXX      XXXXXXXXXXXX      XXXXXXOOOOOO
//      XXXXXXOOOOOO      XXXXXXXXXXXX      XXXXOOOOXXXX      OOOOOOOOOOOO      XXXXXXOOOOOO
//      XXXXXXOOOOOO      OOOOOOOOOOOO      XXXXOOOOXXXX      OOOOOOOOOOOO      OOOOOOXXXXXX
//      XXXXXXOOOOOO      OOOOOOOOOOOO      XXXXOOOOXXXX      XXXXXXXXXXXX      OOOOOOXXXXXX
//      XXXXXXOOOOOO      OOOOOOOOOOOO      XXXXOOOOXXXX      XXXXXXXXXXXX      OOOOOOXXXXXX
//         type 1            type 3            type 2            type 4            type 5


void compute_features_parameters(uint32_t image_height, uint32_t image_width, std::vector<std::vector<fparam_t>>& parameters) 
{
	#ifdef ENABLE_FEATURE_TYPE_1

	// Compute features parameters of type 1
	for (fparam_t i = 0; i < image_height; i++) 
	{
		for (fparam_t j = 0; j < image_width; j++) 
		{
			for (fparam_t h = 0; h < image_height - i; h++) 
			{
				for (fparam_t w = 0; w < (image_width - j) / 2; w++) 
				{
					std::vector<fparam_t> feature_parameters;
					feature_parameters.push_back(1); // Type of the feature
					feature_parameters.push_back(i);
					feature_parameters.push_back(j);
					feature_parameters.push_back(h);
					feature_parameters.push_back(w);
					parameters.push_back(feature_parameters);
				}
			}
		}
	}

	#endif

	#ifdef ENABLE_FEATURE_TYPE_2

	// Compute features parameters of type 2
	for (fparam_t i = 0; i < image_height; i++) 
	{
		for (fparam_t j = 0; j < image_width; j++) 
		{
			for (fparam_t h = 0; h < image_height - i; h++) 
			{
				for (fparam_t w = 0; w < (image_width - j) / 3; w++) 
				{
					std::vector<fparam_t> feature_parameters;
					feature_parameters.push_back(2); // Type of the feature
					feature_parameters.push_back(i);
					feature_parameters.push_back(j);
					feature_parameters.push_back(h);
					feature_parameters.push_back(w);
					parameters.push_back(feature_parameters);
				}
			}
		}
	}

	#endif 

	#ifdef ENABLE_FEATURE_TYPE_3

	// Compute features parameters of type 3
	for (fparam_t i = 0; i < image_height; i++) 
	{
		for (fparam_t j = 0; j < image_width; j++) 
		{
			for (fparam_t h = 0; h < (image_height - i) / 2; h++) 
			{
				for (fparam_t w = 0; w < image_width - j; w++) 
				{
					std::vector<fparam_t> feature_parameters;
					feature_parameters.push_back(3); // Type of the feature
					feature_parameters.push_back(i);
					feature_parameters.push_back(j);
					feature_parameters.push_back(h);
					feature_parameters.push_back(w);
					parameters.push_back(feature_parameters);
				}
			}
		}
	}

	#endif 

	#ifdef ENABLE_FEATURE_TYPE_4

	// Compute features parameters of type 4
	for (fparam_t i = 0; i < image_height; i++) 
	{
		for (fparam_t j = 0; j < image_width; j++) 
		{
			for (fparam_t h = 0; h < (image_height - i) / 3; h++) 
			{
				for (fparam_t w = 0; w < image_width - j; w++) 
				{
					std::vector<fparam_t> feature_parameters;
					feature_parameters.push_back(4); // Type of the feature
					feature_parameters.push_back(i);
					feature_parameters.push_back(j);
					feature_parameters.push_back(h);
					feature_parameters.push_back(w);
					parameters.push_back(feature_parameters);
				}
			}
		}
	}

	#endif

	#ifdef ENABLE_FEATURE_TYPE_5

	// Compute features parameters of type 5
	for (fparam_t i = 0; i < image_height; i++) 
	{
		for (fparam_t j = 0; j < image_width; j++) 
		{
			for (fparam_t h = 0; h < (image_height - i) / 2; h++) 
			{
				for (fparam_t w = 0; w < (image_width - j) / 2; w++) 
				{
					std::vector<fparam_t> feature_parameters;
					feature_parameters.push_back(5); // Type of the feature
					feature_parameters.push_back(i);
					feature_parameters.push_back(j);
					feature_parameters.push_back(h);
					feature_parameters.push_back(w);
					parameters.push_back(feature_parameters);
				}
			}
		}
	}

	#endif
}

void compute_features_values(	uint32_t image_height, uint32_t image_width, 
								const Matrix<iicoef_t> *integralimages, uint32_t ii_index, 
								Matrix<fvalue_t> &features_values) 
{
	uint32_t feature_index = 0;

	#ifdef ENABLE_FEATURE_TYPE_1

	// Compute features values of type 1 applying on image ii_index
	for (uint32_t i = 0; i < image_height; i++) 
	{
		for (uint32_t j = 0; j < image_width; j++) 
		{
			for (uint32_t h = 0; h < image_height - i; h++) 
			{
				for (uint32_t w = 0; w < (image_width - j) / 2; w++) 
				{
					double S1 = compute_pixels_sum(integralimages[ii_index], i, j, i + h, j + w);
					double S2 = compute_pixels_sum(integralimages[ii_index], i, j + w, i + h, j + 2 * w);
					features_values.set(S1 - S2, feature_index, ii_index);
					feature_index += 1;
				}
			}
		}
	}

	#endif

	#ifdef ENABLE_FEATURE_TYPE_2

	//  Compute features values of type 2 applying on image k
	for (uint32_t i = 0; i < image_height; i++) 
	{
		for (uint32_t j = 0; j < image_width; j++) 
		{
			for (uint32_t h = 0; h < image_height - i; h++) 
			{
				for (uint32_t w = 0; w < (image_width - j) / 3; w++) 
				{
					double S1 = compute_pixels_sum(integralimages[ii_index], i, j, i + h, j + w);
					double S2 = compute_pixels_sum(integralimages[ii_index], i, j + w, i + h, j + 2 * w);
					double S3 = compute_pixels_sum(integralimages[ii_index], i, j + 2 * w, i + h, j + 3 * w);
					features_values.set(S1 - S2 + S3, feature_index, ii_index);
					feature_index += 1;
				}
			}
		}
	}

	#endif

	#ifdef ENABLE_FEATURE_TYPE_3

	//  Compute features values of type 3 applying on image ii_index
	for (uint32_t i = 0; i < image_height; i++) 
	{
		for (uint32_t j = 0; j < image_width; j++) 
		{
			for (uint32_t h = 0; h < (image_height - i) / 2; h++) 
			{
				for (uint32_t w = 0; w < image_width - j; w++) 
				{
					double S1 = compute_pixels_sum(integralimages[ii_index], i, j, i + h, j + w);
					double S2 = compute_pixels_sum(integralimages[ii_index], i + h, j, i + 2 * h, j + w);
					features_values.set(S1 - S2, feature_index, ii_index);
					feature_index += 1;
				}
			}
		}
	}

	#endif

	#ifdef ENABLE_FEATURE_TYPE_4

	//  Compute features values of type 4 applying on image ii_index
	for (uint32_t i = 0; i < image_height; i++) 
	{
		for (uint32_t j = 0; j < image_width; j++) 
		{
			for (uint32_t h = 0; h < (image_height - i) / 3; h++) 
			{
				for (uint32_t w = 0; w < image_width - j; w++) 
				{
					double S1 = compute_pixels_sum(integralimages[ii_index], i, j, i + h, j + w);
					double S2 = compute_pixels_sum(integralimages[ii_index], i + h, j, i + 2 * h, j + w);
					double S3 = compute_pixels_sum(integralimages[ii_index], i + 2 * h, j, i + 3 * h, j + w);
					features_values.set(S1 - S2 + S3, feature_index, ii_index);
					feature_index += 1;
				}
			}
		}
	}

	#endif

	#ifdef ENABLE_FEATURE_TYPE_5

	//  Compute features values of type 5 applying on image ii_index
	for (uint32_t i = 0; i < image_height; i++) 
	{
		for (uint32_t j = 0; j < image_width; j++) 
		{
			for (uint32_t h = 0; h < (image_height - i) / 2; h++) 
			{
				for (uint32_t w = 0; w < (image_width - j) / 2; w++) 
				{
					double S1 = compute_pixels_sum(integralimages[ii_index], i, j, i + h, j + w);
					double S2 = compute_pixels_sum(integralimages[ii_index], i + h, j, i + 2 * h, j + w);
					double S3 = compute_pixels_sum(integralimages[ii_index], i, j + w, i + h, j + 2 * w);
					double S4 = compute_pixels_sum(integralimages[ii_index], i + h, j + w, i + 2 * h, j + 2 * w);
					features_values.set(S1 - S2 - S3 + S4, feature_index, ii_index);
					feature_index += 1;
				}
			}
		}
	}

	#endif

}


