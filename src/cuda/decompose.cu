#include <cstdlib>
#include <cstdio>
#include <cstdint>
#include <iostream>
#include <chrono>
#include <ctime>


#define IMG_HEIGHT 	100
#define IMG_WIDTH  	100

#define SAMPLE_HEIGHT 	24
#define SAMPLE_WIDTH 	24
#define SAMPLE_SIZE		4		// SAMPLE_WIDTH * SAMPLE_HEIGHT


#define cudaCheckError() {                                                                       \
        cudaError_t e=cudaGetLastError();                                                        \
        if (e!=cudaSuccess) {                                                                    \
            printf("Cuda failure %s:%d: '%s'\n",__FILE__,__LINE__,cudaGetErrorString(e));        \
            exit(EXIT_FAILURE);                                                                  \
        }                                                                                        \
    }


__global__ void extract_window(uint8_t *image, uint8_t *windows)
{
	if (threadIdx.y < SAMPLE_HEIGHT && threadIdx.x < SAMPLE_WIDTH)
	{
		int blockId_windows 	= blockIdx.x + blockIdx.y * gridDim.x;
		int threadId_windows	= blockId_windows * (SAMPLE_WIDTH * SAMPLE_HEIGHT) + (threadIdx.y * SAMPLE_WIDTH) + threadIdx.x;

		int blockId_image		= blockIdx.x + blockIdx.y * IMG_WIDTH;
		int threadId_image		= blockId_image + threadIdx.y * IMG_WIDTH + threadIdx.x;
		
		windows[threadId_windows] = image[threadId_image];
	}
}


int main(int argc, char** argv)
{

    std::chrono::high_resolution_clock::time_point start, end;
    double gpu_copy_time = 0;
    double gpu_exec_time = 0;
    double cpu_copy_time = 0;
    double cpu_exec_time = 0;

	uint8_t* h_array = new uint8_t[IMG_HEIGHT * IMG_WIDTH];
	for (int i = 0; i < IMG_HEIGHT * IMG_WIDTH; i++) { h_array[i] = i % 256; }

	// Initializing variables
	uint8_t* d_image;
	uint8_t* d_windows;

	int d_image_size = IMG_WIDTH * IMG_HEIGHT;
	int d_windows_size = (IMG_WIDTH + 1 - SAMPLE_WIDTH) * (IMG_HEIGHT + 1 - SAMPLE_HEIGHT) * SAMPLE_HEIGHT * SAMPLE_WIDTH;

    start = std::chrono::high_resolution_clock::now();

	cudaMalloc(&d_image, d_image_size * sizeof(uint8_t));
	cudaMemcpy(d_image, h_array, d_image_size * sizeof(uint8_t), cudaMemcpyHostToDevice);

	cudaMalloc(&d_windows, d_windows_size * sizeof(uint8_t));

    end = std::chrono::high_resolution_clock::now();
    gpu_copy_time += std::chrono::duration_cast<std::chrono::duration<double>>(end - start).count();

	// Computing number of blocks

	std::cout << "blocks dim: " << IMG_WIDTH + 1 - SAMPLE_WIDTH << ' ' << IMG_HEIGHT + 1 - SAMPLE_HEIGHT << std::endl;
	dim3 blocks(IMG_WIDTH + 1 - SAMPLE_WIDTH, IMG_HEIGHT + 1 - SAMPLE_HEIGHT);
	dim3 threads(32, 32);

    start = std::chrono::high_resolution_clock::now();

	// Get all 24x24 subwindows from the initial image
	extract_window<<<blocks, threads>>>(d_image, d_windows);

	end = std::chrono::high_resolution_clock::now();
    gpu_exec_time += std::chrono::duration_cast<std::chrono::duration<double>>(end - start).count();

    std::cout << "GPU elapsed time: copy:" << gpu_copy_time << " seconds - execution: " << gpu_exec_time << " seconds" << std::endl;

    start = std::chrono::high_resolution_clock::now();

	uint8_t* h_windows = new uint8_t[d_windows_size];
	cudaMemcpy(h_windows, d_windows, d_windows_size * sizeof(uint8_t), cudaMemcpyDeviceToHost);
    
    end = std::chrono::high_resolution_clock::now();
    gpu_copy_time += std::chrono::duration_cast<std::chrono::duration<double>>(end - start).count();

	// Compare with CPU code

    start = std::chrono::high_resolution_clock::now();

	uint8_t* seq_windows = new uint8_t[d_windows_size];

	end = std::chrono::high_resolution_clock::now();

	cpu_copy_time += std::chrono::duration_cast<std::chrono::duration<double>>(end - start).count();

    start = std::chrono::high_resolution_clock::now();

	for (uint32_t i = 0; i < IMG_HEIGHT + 1 - SAMPLE_HEIGHT; i++)
	{
		for (uint32_t j = 0; j < IMG_WIDTH + 1 - SAMPLE_WIDTH; j++)
		{
			for (uint32_t n = 0; n < SAMPLE_HEIGHT; n++) 
			{
				for (uint32_t m = 0; m < SAMPLE_WIDTH; m++)
				{
					uint32_t window_idx 	= (j + i * (IMG_WIDTH + 1 - SAMPLE_WIDTH)) * (SAMPLE_WIDTH * SAMPLE_HEIGHT) + (n * SAMPLE_WIDTH) + m;
					uint32_t image_idx 		= j + i * IMG_WIDTH + n * IMG_WIDTH + m;
					seq_windows[window_idx]	= h_array[image_idx];
				}
			}
		}
	}

	end = std::chrono::high_resolution_clock::now();
    cpu_exec_time += std::chrono::duration_cast<std::chrono::duration<double>>(end - start).count();
    
    std::cout << "CPU elapsed time: copy:" << cpu_copy_time << " seconds - execution: " << cpu_exec_time << " seconds" << std::endl;

	// Check if CPU & GPU results are identical

	bool same_results = true;
	for (uint32_t i = 0; i < d_windows_size; i++)
	{
		if (h_windows[i] != seq_windows[i])
		{
			same_results = false;
			break;
		}
	}

	std::cout << (same_results ? "results are equal" : "results are different") << std::endl;

	cudaFree(d_image);
	cudaFree(d_windows);
	delete[] h_array;
	delete[] h_windows;

	return 0;
}


