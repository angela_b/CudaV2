#include <iostream>
#include <vector>
#include <cstdint>
#include "parallelMatrix.h"
#include "macros.hpp"

#include "image.hpp"
#include "matrix.hpp"
#include "weak_classifier.hpp"
#include "strong_classifier.hpp"
#include "adaboost.hpp"


int main(int argc, char** argv) {

	if (argc != 3) {
		std::cerr << "Usage: ./ViolaJonesSequential path/to/positives_samples_urls.txt path/to/negatives_samples_urls.txt" << std::endl;
		return 1;
	}

	std::string positives_samples_urls_file_path(argv[1]);
	std::string negatives_samples_urls_file_path(argv[2]);

	std::vector<Matrix<iicoef_t> *> positive_samples;
	std::vector<Matrix<iicoef_t> *> negative_samples;

	// Loading samples
	LOG(std::cerr << "[INFO] " << "loading positive samples from: " << positives_samples_urls_file_path << std::endl)
	readimages(positives_samples_urls_file_path, positive_samples);
	LOG(std::cerr << "[INFO] " << positive_samples.size() << " images loaded." << std::endl)
	LOG(std::cerr << "[INFO] " << "loading negative samples from: " << negatives_samples_urls_file_path << std::endl)
	readimages(negatives_samples_urls_file_path, negative_samples);
	LOG(std::cerr << "[INFO] " << negative_samples.size() << " images loaded." << std::endl)

	// Computation of integral images
	Matrix<iicoef_t> *integral_positive_samples = new Matrix<iicoef_t>[positive_samples.size()];
	Matrix<iicoef_t> *integral_negative_samples = new Matrix<iicoef_t>[negative_samples.size()];

	double * res_Integer;
	LOG(std::cerr << "[INFO] " << "computing integral images for positive samples..." << std::endl)
//	#ifndef CUDA_PARALLEL_INTEGRAL

	for (uint32_t i = 0; i < positive_samples.size(); i++) {
	 get_integralimage(*positive_samples.at(i), integral_positive_samples[i]); 
	}
	LOG(std::cerr << "[INFO] " << positive_samples.size() << " integral images computed." << std::endl)
	LOG(std::cerr << "[INFO] " << "computing integral images for negative samples..." << std::endl)
	for (uint32_t i = 0; i < negative_samples.size(); i++) {
	 get_integralimage(*negative_samples.at(i), integral_negative_samples[i]); 
	}
	LOG(std::cerr << "[INFO] " << negative_samples.size() << " integral images computed." << std::endl)
	

	uint32_t n_positive_samples = positive_samples.size();
	uint32_t n_negative_samples = negative_samples.size();
	// Free resources used by the samples, we don't need them anymore because we got the integral images
	std::cerr << "[INFO] " << "freeing resources used by the samples" << std::endl;
	for (Matrix<iicoef_t>* image : positive_samples) { delete image; }
	for (Matrix<iicoef_t>* image : negative_samples) { delete image; }

	// Computation of features parameters
	std::vector<std::vector<fparam_t>> features_parameters;
	Matrix<fvalue_t> positives_features_values;
	Matrix<fvalue_t> negatives_features_values;

	LOG(std::cerr << "[INFO] " << "computing the features parameters for images with dimensions: " << SAMPLE_WIDTH << 'x' << SAMPLE_HEIGHT << std::endl)
	compute_features_parameters(SAMPLE_HEIGHT, SAMPLE_WIDTH, features_parameters);

	LOG(std::cerr << "[INFO] " << "applying the features on positive samples..." << std::endl)
	compute_features_values(SAMPLE_HEIGHT, SAMPLE_WIDTH, integral_positive_samples, n_positive_samples, positives_features_values, features_parameters);
	LOG(std::cerr << "[INFO] " << "applied " << features_parameters.size() << " features for " << n_positive_samples << " positives samples." << std::endl)
	LOG(std::cerr << "[INFO] " << "applying the features on negative samples..." << std::endl)
	compute_features_values(SAMPLE_HEIGHT, SAMPLE_WIDTH, integral_negative_samples, n_negative_samples, negatives_features_values, features_parameters);
	LOG(std::cerr << "[INFO] " << "applied " << features_parameters.size() << " features for " << n_negative_samples << " negatives samples." << std::endl)

	LOG(std::cerr << "[INFO] " << "freeing resources used by the integral images" << std::endl)
	delete[] integral_positive_samples;
	delete[] integral_negative_samples;

	StrongClassifier strong_classifier;
	LOG(std::cerr << "[INFO] " << "applying adaboost algorithm" << std::endl)
	adaboost(strong_classifier, 10, positives_features_values, negatives_features_values, features_parameters);
	LOG(std::cerr << "[INFO] " << "adaboost algorithm complete" << std::endl)

	LOG(std::cerr << "[INFO] " << "strong classifier coefficients: " << std::endl)
	LOG(std::cerr << strong_classifier._coefficients << std::endl)

	LOG(std::cerr << "[INFO] " << "testing the computed string classifier..." << std::endl)

	uint32_t rightpositives 	= 0;
	uint32_t rightnegatives		= 0;
	uint32_t falsepositives 	= 0;
	uint32_t falsenegatives		= 0;

	for (uint32_t j1 = 0; j1 < positives_features_values._w; j1++) {
		double res = strong_classifier.process(positives_features_values, j1);
		if (res == 1) { rightpositives += 1; }
		else { falsenegatives += 1; }
		LOG(std::cerr 	<< '\r' 
						<<  "right positives: "		<< rightpositives
						<< ", right negatives: " 	<< rightnegatives
						<< ", false positives: "	<< falsepositives
						<< ", false negatives: " 	<< falsenegatives
						<< std::flush)

	}

	for (uint32_t j2 = 0; j2 < negatives_features_values._w; j2++) {
		double res = strong_classifier.process(negatives_features_values, j2);
		if (res == 0) { rightnegatives += 1; }
		else { falsepositives += 1; }
		LOG(std::cerr 	<< '\r' 
						<<  "right positives: "		<< rightpositives
						<< ", right negatives: " 	<< rightnegatives
						<< ", false positives: "	<< falsepositives
						<< ", false negatives: " 	<< falsenegatives
						<< std::flush)

	}
	LOG(std::cerr << std::endl)

	LOG(std::cerr << "[INFO] " << "end" << std::endl)

	return 0;
}

