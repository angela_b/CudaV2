#include "weak_classifier_parallel.hpp"
#include <stdlib.h>     /* srand, rand */
#include <time.h>
#include "matrix.hpp"
#include "macros.h"
#include "weak_classifier.hpp"




/** Calcul en CUDA et CPU la vitesse d'entrainement des weaks classifiers sur une itération **/ 
/** Les paramètres des fonctions et macro se trouvent dans le fichier macros.hpp dans le dossier src/ **/


/** Genere un tableau de nombre aléatoire entre 0 et 1000, tableau de taille sizeX x sizeY **/ 


/*
** Comme son nom l'indique, prend en argument un tableau de fvalue et le nombre d'element et copie en mémoire cuda le tableau après l'avoir allouer
** HostArray est le tableau a copier
** NumElement est le nombre d'élément du tableau
** Renvoie le pointeur Cuda pointant vers notre tableau en GPU
** Assure la gestion d'erreur du malloc et du Memcopy
*/
fvalue_t *CopyArrayToGPU(fvalue_t *HostArray, fvalue_t NumElements)
{
    fvalue_t bytes = sizeof(fvalue_t) * NumElements;
    void *DeviceArray;

    // Allocate memory on the GPU for array
    auto error_code = cudaMalloc(&DeviceArray, bytes);
    if (error_code != cudaSuccess)
    {
        LOG(std::cerr << "[CopyArrayToGPU] CopyArrayToGPU(): Couldn't allocate mem for array on GPU." << std::endl)
        return NULL;
    }

    // Copy the contents of the host array to the GPU
    if (cudaMemcpy(DeviceArray, HostArray, bytes, cudaMemcpyHostToDevice) != cudaSuccess)
    {
        LOG(std::cerr << "CopyArrayToGPU(): Couldn't copy host array to GPU." << std::endl)
        cudaFree(DeviceArray);
        return NULL;
    }
    return (fvalue_t *)DeviceArray;
}
fvalue_t * computeRandom(fvalue_t sizeX, fvalue_t sizeY)
{

    fvalue_t* arrayOnes = new fvalue_t [sizeX * sizeY];
    for (fvalue_t i = 0; i < sizeX * sizeY; i++)
    {
        arrayOnes[i] = 1 * rand() % 1000 + 1;
    }

    return arrayOnes;

}


fvalue_t main(void)
{
   
/* Matrice qui vont simuler les matrices de features positives et négatives */ 
    Matrix<fvalue_t>  positive(NB_FEATURE, NB_IMAGE);
    Matrix<fvalue_t>  negativ(NB_FEATURE, NB_IMAGE);
    positive._array = computeRandom(NB_IMAGE, NB_FEATURE);
    negativ._array = computeRandom(NB_IMAGE ,NB_FEATURE);
   
    fvalue_t best;
    int sign;
    float temps;
    clock_t t1, t2;
    fvalue_t nb_feature = NB_FEATURE; 
    LOG( std::cout << "[TestWeak] Entrainement des " << NB_FEATURE << " weaks classifiers en CUDA " << std::endl)
    t1 = clock();
 
    uint32_t * d_n_feature;
    int err = cudaMalloc((void **)&d_n_feature, sizeof(uint32_t));
    LOG(std::cout << "Error " << err  << std::endl)
    
    err = cudaMemcpy(d_n_feature, &nb_feature, sizeof(uint32_t), cudaMemcpyHostToDevice); /* Transfer size to GPU */
    LOG(std::cout << "Error " << err  << std::endl )
    uint32_t width_image = NB_IMAGE * 2; //3
    uint32_t * d_width_image;
    err = cudaMalloc((void **)&d_width_image, sizeof(uint32_t));
    err= cudaMemcpy(d_width_image, &width_image, sizeof(uint32_t), cudaMemcpyHostToDevice); /* Transfer size to GPU */
    LOG(std::cout << "Error " << err  << std::endl)
    
    fvalue_t * d_Answer_retriever = new fvalue_t[NB_IMAGE * nb_feature];
    fvalue_t error = cudaMalloc((void **)&d_Answer_retriever, sizeof(fvalue_t)* NB_IMAGE * nb_feature);
    LOG(std::cout << "Error cuda malloc answer retrieve : "  << error << std::endl)
    
    fvalue_t *array_positive = positive._array; /* Autant de valeur par feature que de sample en ligne, autant de sample * nb_feature en colonne*/
    fvalue_t *array_negative = negativ._array;
    /* Copy en GPU des matrices */ 
    auto d_positiv = CopyArrayToGPU(array_positive, NB_IMAGE * nb_feature) ;
    auto d_negativ = CopyArrayToGPU(array_negative, NB_IMAGE * nb_feature) ;

    uint32_t width = NB_IMAGE * 2; /* Nombre d'image est la somme des positives et négatives, pour simplifier ici on considere avoir autant de négatifs que de positifs*/
    uint32_t * d_width;
    err = cudaMalloc((void **)&d_width, sizeof(uint32_t));
    LOG(std::cout << "Error " << err  << std::endl )
    err = cudaMemcpy(d_width, &width, sizeof(uint32_t), cudaMemcpyHostToDevice); /* Transfer size to GPU */
    LOG(std::cout << "Error " << err  << std::endl)

    uint32_t nb_positif = NB_IMAGE ; /* Nombre d'image positives*/
    uint32_t * d_nb_positive;
    err = cudaMalloc((void **)&d_nb_positive, sizeof(uint32_t));
    LOG(std::cout << "Error " << err  << std::endl )

    err = cudaMemcpy(d_nb_positive, &nb_positif, sizeof(uint32_t), cudaMemcpyHostToDevice); /* Transfer size to GPU */
    LOG(std::cout << "Error " << err  << std::endl)
    int nb_bloc = nb_feature / ( NB_IMAGE* 2) ;
    LOG(std::cout << "Ask for " << nb_bloc << "bloc " << std::endl)
    Second_train_weak_classifier_p <<< nb_bloc, 1024 >>> (d_Answer_retriever,
    d_positiv, /** Matrix 2D **/
    d_negativ,
    d_width,
     d_nb_positive,
    d_n_feature);



    /* Retrieve */
    fvalue_t * experiment = new fvalue_t [nb_bloc * 1024 ]; 
    /* Recuperation de l'fvalue_teger image  */
    fvalue_t  res_error= cudaMemcpy(experiment, d_Answer_retriever,nb_bloc * 1024  * sizeof(fvalue_t), cudaMemcpyDeviceToHost);/*  SIZE taille matrice * 2 car negatif * 2 car fvalue_t signe */ 
    std::cout << "Error " << res_error << std::endl;
    t2 = clock();
    temps = (float)(t2-t1)/CLOCKS_PER_SEC;
    printf("temps d'execution = %f\n", temps);
    LOG(std::cerr << std::endl <<  "Finish" << std::endl)


 
    t1 = clock();
 
     
    LOG(std::cout << "[TestWeak] Entrainement des " << NB_FEATURE << " weaks classifiers  SEQUENTIEL " << std::endl)
    for (int i = 0; i < NB_FEATURE; i++)
    {
        train_weak_classifier( i, positive, negativ, 
                            best, sign ); 
    }
    t2 = clock();
    temps = (float)(t2-t1)/CLOCKS_PER_SEC;
    printf("temps d'execution = %f\n", temps);
    
    /** Simulation du nombre de feature **/ 

}
