#ifndef __WEAK_CLASSIFIER_PARALLEL_HPP__
#define __WEAK_CLASSIFIER_PARALLEL_HPP__

#include <cstdint>

#include "matrix.hpp"

			/** GPU **/

/** Compute every weak classifier at one **/ 
__global__ void Second_train_weak_classifier_p(fvalue_t * Answer,
	fvalue_t *positives_features_values, /**  N x nb_feature image positive **/
	fvalue_t *negatives_features_values, /** M x nb_feature image negative */ 
	uint32_t *width_array, /** Nombre de colonne de la matrice **/ 
	uint32_t * number_of_positives,
	uint32_t  * nb_feature /* Nombre de ligne de la matrice */ 
	) ;
			/** CPU **/
#endif /* __WEAK_CLASSIFIER_PARALLEL_HPP__ */