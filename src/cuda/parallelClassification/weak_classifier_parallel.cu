#include "weak_classifier_parallel.hpp"
#include <vector>
#include <limits.h>
// Given a threshold and a sign associed to this threshold, return the estimated class for a value

/** CPU TO COMPARE -**/

#include "weak_classifier_parallel.hpp"


/** GPU **/


/** Accesseur pour récupérer les variables dans un tableau 2d stocké sous forme 1D **/
__device__ double posAt(double * array, uint32_t w, uint32_t i, uint32_t j)
{

	return array[i * w + j]; // NbFeature * Taille ligne  + col
}





/* Caler un max de feature par bloc */
__global__ void Second_train_weak_classifier_p(fvalue_t * Answer,
	fvalue_t *positives_features_values, /**  N x nb_feature image positive **/
	fvalue_t *negatives_features_values, /** M x nb_feature image negative */ 
	uint32_t *width_array, /* Nombre positif + nombre negatif */ 
	uint32_t * number_of_positives, /* Necessaire car le nombre de positif n'est pas forcement egal au négatif **/ 
	uint32_t  * nb_feature /* Nombre de ligne */ 
	) 
{ 
	int nb_feature_max_per_block = blockDim.x / (*width_array);

	int index = threadIdx.x + blockIdx.x * blockDim.x;
	int decalage =blockIdx.x * ( blockDim.x - *width_array * nb_feature_max_per_block); /* Calcul de la perte des N threads jete car feature ne rentre pas dans le block */

 	Answer[index] = INT_MAX; /** Initialise tous la reponse **/ 
	if (index >=  *width_array * *nb_feature)
 		return;	
 	/* Init en shared */ 
 	fvalue_t my_threshold = 0;
	__shared__ fvalue_t *bucket_error;
	
 	int false_index = index - decalage;
 	/** Calcul de la feature courante pour le thread **/ 
 	int my_feature = (false_index ) / *width_array;
 	if (index  == 0)
 	{
 		printf("Affichage paramètre : \nwidth array %i\nnumber of positive%i\nnb_feature %i\nFeature per block %i\n",
 		 *width_array, *number_of_positives, *nb_feature, nb_feature_max_per_block);
 	}
 	__syncthreads();

 	/* Chaque groupe de N thread recupere une valeur dans les tableau */
 	if (threadIdx.x % *width_array < *number_of_positives) /* Positifs */ 
 	{
 		my_threshold = positives_features_values[*number_of_positives * my_feature + threadIdx.x % *number_of_positives];

 	}
 	else /* Les WIDTH - Nb Positif recuperent dans les négatifs **/ 
 	{
 		my_threshold = negatives_features_values[ (*width_array - *number_of_positives) * my_feature +  threadIdx.x % (*width_array - *number_of_positives)];
 	}
 	int sign [] = { -1 , 1 }; /* Calcul de l'erreur positive et de l'erreur négative **/
 	int my_error = 0; 
 	
	if (threadIdx.x == 0)
 	{
 		/** Initialisation de la shared **/
	    bucket_error = new int [blockDim.x];
 		for (int i = 0; i < blockDim.x ; i++)
 			bucket_error[i] = INT_MAX;
 	}
 	__syncthreads();
  /* Parcourt des N images de la feature pour calculer les erreurs positives et négatives */ 
 	for (int signe : sign) {
 		my_error = 0;
	 	for (int i = 0; i < *number_of_positives; i++)
	 	{
	 		fvalue_t value = positives_features_values[*number_of_positives * my_feature + i];
	 		fvalue_t res_positive = signe * value >= signe * my_threshold ? 1 : -1;
	 			
	 				if (res_positive != 1) { my_error ++;} 		
	 	}

	 	for (int i = 0; i < *width_array  - *number_of_positives; i++)
	 	{
	 			fvalue_t value = negatives_features_values[ (*width_array - *number_of_positives) * my_feature + i ];
	 			fvalue_t res_negative = signe * value >= signe * my_threshold ? 1 : -1;
	 			if (res_negative != -1) { my_error ++;} 				
	 	}
	 	
	 	if (my_error < abs(bucket_error[index % blockDim.x]) )
	 	{
	 			bucket_error[index % blockDim.x] = signe * my_error; /* SI error negative alors signe negatif */    
		}
 	}
 	__syncthreads();
 	/* Est ce que le thread travaille bien sur une image */ 
 		Answer[index] = bucket_error[threadIdx.x]; /* Si oui alors retient son erreur */
 	if (index == 0)
 		printf("Terminaison Ok \n");
__syncthreads();
 	
}






