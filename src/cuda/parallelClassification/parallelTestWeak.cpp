
#include <stdlib.h>     /* srand, rand */
#include <time.h>
#include "matrix.hpp"
#include "macros.hpp"
#include <cstdint>
#include <stdint.h>
#include "weak_classifier.hpp"



/** Calculeen openMP la vitesse d'entrainement des weaks classifiers sur une itération **/ 
/** Les paramètres des fonctions et macro se trouvent dans le fichier macros.hpp dans le dossier src/ **/


/** Genere un tableau de nombre aléatoire entre 0 et 1000, tableau de taille sizeX x sizeY **/ 
int * computeRandom(int sizeX, int sizeY)
{

    int* arrayOnes = new int [sizeX * sizeY];
    for (int i = 0; i < sizeX * sizeY; i++)
    {
        arrayOnes[i] = 1 * rand() % 1000 + 1;
    }

    return arrayOnes;

}


int main(void)
{
    /** Matrice qui simulent les matrices de features positives et ngatives **/ 
    Matrix<int>  positive(NB_FEATURE, NB_IMAGE);
    Matrix<int>  negativ(NB_FEATURE, NB_IMAGE);

    positive._array = computeRandom(NB_IMAGE, NB_FEATURE);
    negativ._array = computeRandom(NB_IMAGE ,NB_FEATURE);
    
    int best;
    int sign;
    
    float temps;
    clock_t t1, t2;
    t1 = clock();     
    std::cout << "[TestWeak] Entrainement des " << NB_FEATURE << " weaks classifiers  SEQUENTIEL devenu PARALLELE" << std::endl;
    #pragma omp parallel for
    for (int i = 0; i < NB_FEATURE; i++)
    {
        train_weak_classifier( i, positive, negativ, 
                            best, sign ); 
    }
    t2 = clock();
    temps = (float)(t2-t1)/CLOCKS_PER_SEC;
    printf("temps d'execution = %f\n", temps);
    LOG(std::cerr << std::endl <<  "Finish" << std::endl)

}
