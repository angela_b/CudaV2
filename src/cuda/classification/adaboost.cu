#include "adaboost.hpp"
#include "weak_classifier_parallel.hpp"

#include <tuple>
/* Recupere le min et son index pour chaque feature */ 
struct structo {fvalue_t * result; fvalue_t* index;};

/* Calcule la position et la valeur du min pour chaque feature en parcourant la matrice renvoyée par la GPU */
structo computeMin(fvalue_t *array, int nb_bloc, int nb_thread, int N, int n_feature, int nb_positif)
{
	fvalue_t *resultat = new fvalue_t[n_feature];/* La valeur d'erreur */
	fvalue_t *resultat_index = new fvalue_t[n_feature]; /* Indice des best error */
	
	for (int i = 0; i < n_feature; i++)
	{

		resultat_index[i] = nb_thread;
		resultat[i] = nb_thread;
	}
	int cpt = 0;	
	int nb_max_feature = 1024 / N; /* Nombre de feature par block */ 
	for(int i = 0; i < nb_bloc; i++)
	{
		for (int j = 0; j < N * nb_max_feature; j++) /* Toutes les valeurs utilisés par des threads travailleurs */
		{
			int current_feature = i * nb_max_feature + j / N;
			int decalage = 0;
			int my_value = array[i * nb_thread + j + decalage];
			if (abs(my_value) < abs(resultat[current_feature]))
			{
				resultat[current_feature] = my_value;
				int index_in_feature = j % N;			
				resultat_index[current_feature] = index_in_feature + N * current_feature;
			}
		}
		
	}
    return structo{resultat,resultat_index};
	
}
void cudaCheckError() {                                                                       
        cudaError_t e=cudaGetLastError();                                                        
        if(e!=cudaSuccess) {                                                                     
            printf("Cuda failure  '%s'\n",cudaGetErrorString(e));        
            exit(EXIT_FAILURE);                                                                  
        }                                                                                        
    }


fvalue_t *CopyArrayToGPU(fvalue_t *HostArray, int NumElements)
{
    int bytes = sizeof(fvalue_t) * NumElements;
    void *DeviceArray;

    // Allocate memory on the GPU for array
    auto error_code = cudaMalloc(&DeviceArray, bytes);
    if (error_code != cudaSuccess)
    {
        LOG(std::cerr << "[CopyArrayToGPU] CopyArrayToGPU(): Couldn't allocate mem for array on GPU." << std::endl)
        return NULL;
    }

    // Copy the contents of the host array to the GPU
    if (cudaMemcpy(DeviceArray, HostArray, bytes, cudaMemcpyHostToDevice) != cudaSuccess)
    {
        LOG(std::cerr << "CopyArrayToGPU(): Couldn't copy host array to GPU." << std::endl)
        cudaFree(DeviceArray);
        return NULL;
    }
    return (fvalue_t *)DeviceArray;
}


bool adaboost(	StrongClassifier &result, uint32_t max_periodes, 
				const Matrix<fvalue_t>& train_positives_features_values, const Matrix<fvalue_t>& train_negatives_features_values,
				const Matrix<fvalue_t>& test_positives_features_values, const Matrix<fvalue_t>& test_negatives_features_values,
				const std::vector<std::vector<fparam_t>>& features_parameters) 
{
	uint32_t N = train_positives_features_values._w + train_negatives_features_values._w;
	uint32_t num_features = train_positives_features_values._h;

	Matrix<fvalue_t> weak_classifiers = Matrix<fvalue_t>(num_features, 2);

	LOG(std::cerr << "\t\t-> computing strong classifier" << std::endl)

	// Computation of weak classifiers
	LOG(std::cerr << "\t\t\t-> training weak classifiers" << std::endl)

		/* Integration weak classifiers parallele */


	int nb_thread = 1024;
	int nb_feature_per_block = 1024 / N;
	std::cout << "Nb feature par block " << nb_feature_per_block << std::endl;
	int nb_bloc = num_features / nb_feature_per_block + 1;
   
    /** Copie nombre de features  = Nombre de ligne **/ 
    uint32_t * d_n_feature;
    cudaMalloc((void **)&d_n_feature, sizeof(uint32_t));
    cudaMemcpy(d_n_feature, &num_features, sizeof(uint32_t), cudaMemcpyHostToDevice); /* Transfer size to GPU */
/* La reponse de cuda */    
    fvalue_t * d_Answer_retriever = new fvalue_t[nb_thread * nb_bloc];
    int error = cudaMalloc((void **)&d_Answer_retriever, sizeof(fvalue_t) * nb_bloc * nb_thread);
    LOG(std::cout << "Error cuda malloc answer retrieve : "  << error << std::endl);

    
    auto d_positiv = CopyArrayToGPU(train_positives_features_values._array, train_positives_features_values._w * train_positives_features_values._h) ;
    auto d_negativ = CopyArrayToGPU(train_negatives_features_values._array, train_negatives_features_values._w * train_negatives_features_values._h) ;
    uint32_t * d_width;
    error = cudaMalloc((void **)&d_width, sizeof(uint32_t));
    LOG(std::cout << "Error cuda malloc N: "  << error << std::endl);

    error = cudaMemcpy(d_width, &N, sizeof(uint32_t), cudaMemcpyHostToDevice); /* Transfer size to GPU */
    LOG(std::cout << "Error cuda copy N retrieve : "  << error << std::endl);
    
    uint32_t nb_positif = train_positives_features_values._w ; /* Nombre d'image */
    uint32_t * d_nb_positive;
    error = cudaMalloc((void **)&d_nb_positive, sizeof(uint32_t));
    LOG(std::cout << "Error cuda malloc nbpositif  : "  << error << std::endl);

    error = cudaMemcpy(d_nb_positive, &nb_positif, sizeof(uint32_t), cudaMemcpyHostToDevice); /* Transfer size to GPU */
    LOG(std::cout << "Error cuda Copy nbpositive  : "  << error << std::endl);

	Second_train_weak_classifier_p <<< nb_bloc, nb_thread >>> (d_Answer_retriever,
    d_positiv,
    d_negativ,
    d_width,
     d_nb_positive,
    d_n_feature);
     fvalue_t * experiment = new fvalue_t [nb_thread * nb_bloc]; 
    int  res_error= cudaMemcpy(experiment, d_Answer_retriever,nb_bloc * nb_thread  * sizeof(fvalue_t), cudaMemcpyDeviceToHost);/*  SIZE taille matrice * 2 car negatif * 2 car double signe */ 
    LOG(std::cerr << "[OutputCopy] Resultat cuda copy : " << res_error << std::endl)
    cudaCheckError();
    
    if (res_error -= 0)
    	throw std::out_of_range("Matrix<T>::at() : index out of range, error cuda copy");

    auto tupl = computeMin(experiment, nb_bloc, nb_thread, N, num_features, train_positives_features_values._w);
    auto res = tupl.index;
    /* Liberation de la memoire */ 
    cudaFree(d_Answer_retriever);
    cudaFree(d_positiv);
    cudaFree(d_negativ);
    cudaFree(d_width);
    cudaFree(d_nb_positive);
    cudaFree(d_n_feature);
/* Fin d'integration weak classifiers */
     /* Entrainement des weaks sequentiels */ 
	for (uint32_t i = 0; i < num_features; i++) 
	{
		int value = res[i];
    	fvalue_t threshold;
		fvalue_t sign;
		if (value <= 0)
			sign = -1;
		else
			sign = 1;
 
  	 	if (value % N < train_positives_features_values._w)
    	{
    		threshold = train_positives_features_values._array[res[i]  % N +  i * train_positives_features_values._w ];
    	}	
    	else
    	{
    		threshold = train_negatives_features_values._array[res[i]  % N +  i * train_negatives_features_values._w - train_positives_features_values._w ];
		
    	}
		weak_classifiers.set(threshold, i, 0);
		weak_classifiers.set(sign, i, 1);
		#ifndef PARALLEL
		#endif

	}

	#ifdef PARALLEL
	LOG(std::cerr << '\r' << "\t\t\t\t-> weak_classifiers training" << std::endl)
	#pragma omp parallel for
	#endif
	for (uint32_t i = 0; i < num_features; i++) 
	{
		fvalue_t threshold;
		fvalue_t sign;

		train_weak_classifier(i, train_positives_features_values, train_negatives_features_values, threshold, sign);
		weak_classifiers.set(threshold, i, 0);
		weak_classifiers.set(sign, i, 1);
		#ifndef PARALLEL
		#endif


/* Comparaison weak GPU -* CPU */ 
		int value = res[i];
    	fvalue_t own_threshold;
		fvalue_t own_sign;
		if (value <= 0)
			own_sign = -1;
		else
			own_sign = 1;
 
  	 	if (value % N < train_positives_features_values._w)
    		{
    		own_threshold = train_positives_features_values._array[res[i]  % N +  i * train_positives_features_values._w ];
    	}	
    	else
    	{
    		own_threshold = train_negatives_features_values._array[res[i]  % N +  i * train_negatives_features_values._w - train_positives_features_values._w ];
    		}

    	if (threshold != own_threshold)
    	{	std::cout << std::endl <<   "Error on index " << i << " suppose to found "<<threshold << " and found " << own_threshold 
    	<< " with sign " << sign << " and error " << value <<  std::endl;
	} 	
}


	#ifndef PARALLEL
	LOG(std::cerr << std::endl);
	#endif
	LOG(std::cerr << "\t\t\t-> initializing weights" << std::endl)
	
	Matrix<double> weights = Matrix<double>(N, 1);

	double positives_initial_weights = 1 / (2 * (double)train_positives_features_values._w);
	double negatives_initial_weights = 1 / (2 * (double)train_negatives_features_values._w);

	for (uint32_t i = 0; i < train_positives_features_values._w; i++) 
		weights.set(positives_initial_weights, i, 0);

	for (uint32_t i = train_negatives_features_values._w; i < N; i++)
		weights.set(negatives_initial_weights, i, 0);

	double false_positive_rate 	= 1;
	double true_positive_rate 	= 0;
	uint32_t current_period = 0;
	while (current_period < max_periodes && (false_positive_rate > 0.5 || true_positive_rate < 0.5))
	{
		current_period += 1;
		LOG(std::cerr << "\t\t\t-> period: " << current_period << std::endl)

		// Normalize the weights
		LOG(std::cerr << "\t\t\t\t-> normalize the weights" << std::endl)
		weights *= 1 / weights.sum();

		double min_error = 1;
		uint32_t best_weak_classifier_index = 0;
		
		for (uint32_t i = 0; i < num_features; i++) {
			LOG(std::cerr << '\r' << "\t\t\t\t-> evaluating error of weak classifier " << i + 1 << "/" << num_features << std::flush)
			
			double error = 0;
			for (uint32_t j1 = 0; j1 < train_positives_features_values._w; j1++) 
			{
				error += weights.at(j1, 0) * abs((double)process_weak_classifier(weak_classifiers.at(i, 0), weak_classifiers.at(i, 1), train_positives_features_values.at(i, j1)) - 1);
			}

			Matrix<double> error_neg(train_negatives_features_values._w, 1);
			for (uint32_t j2 = 0; j2 < train_negatives_features_values._w; j2++) 
			{
				error += weights.at(train_positives_features_values._w + j2, 0) * abs((double)process_weak_classifier(weak_classifiers.at(i, 0), weak_classifiers.at(i, 1), train_negatives_features_values.at(i, j2)) - 0);
			}

			if (error < min_error) 
			{
				min_error = error;
				best_weak_classifier_index = i;
			}
		}

		LOG(std::cerr << std::endl)

		LOG(std::cerr 	<< "\t\t\t\t-> best weak classifier found: " << best_weak_classifier_index 
						<< " (type: " 	<< features_parameters[best_weak_classifier_index][0] 
						<< ", i: " 		<< features_parameters[best_weak_classifier_index][1]
						<< ", j; " 		<< features_parameters[best_weak_classifier_index][2]
						<< ", h: " 		<< features_parameters[best_weak_classifier_index][3]
						<< ", w: " 		<< features_parameters[best_weak_classifier_index][4] << ")"
						<< ", error: " << min_error << std::endl)

		if (result._features_indexs.size() > 0 && result._features_indexs[result._features_indexs.size() - 1] == best_weak_classifier_index)
		{
			LOG(std::cerr << "\t\t\t\t-> same feature found as previous period" << std::endl)
			LOG(std::cerr << "\t\t\t-> stopping strong classifier computation" << std::endl)
			return false;
		}

		LOG(std::cerr << "\t\t\t\t-> updating the weights" << std::endl)
		double beta 	= min_error / (1 - min_error);
		double alpha 	= log(1 / beta);
		for (uint32_t j1 = 0; j1 < train_positives_features_values._w; j1++) 
		{
			double w_classifier_res = process_weak_classifier(	weak_classifiers.at(best_weak_classifier_index, 0), 
																weak_classifiers.at(best_weak_classifier_index, 1), 
																train_positives_features_values.at(best_weak_classifier_index, j1));
			double e = abs(w_classifier_res - 1);

			weights.set( weights.at(j1, 0) * exp((1 - e) * log(beta)), j1, 0);
		}

		for (uint32_t j2 = 0; j2 < train_negatives_features_values._w; j2++) 
		{
			double w_classifier_res = process_weak_classifier(	weak_classifiers.at(best_weak_classifier_index, 0), 
																weak_classifiers.at(best_weak_classifier_index, 1), 
																train_negatives_features_values.at(best_weak_classifier_index, j2));
			double e = abs(w_classifier_res - 0);

			weights.set( weights.at(train_negatives_features_values._w + j2, 0) * exp((1 - e) * log(beta)), train_negatives_features_values._w + j2, 0);
		}

		result.add_coefficients(alpha, weak_classifiers.at(best_weak_classifier_index, 0), weak_classifiers.at(best_weak_classifier_index, 1), best_weak_classifier_index);

		// Computation of true positive and false positive rate
		LOG(std::cerr << "\t\t\t\t-> evaluating true positive and false positive rate using test sets" << std::endl)
		double new_false_positive_rate = 0;
		double new_true_positive_rate = 0;
		for (uint32_t k = 0; k < test_negatives_features_values._w; k++)
			new_false_positive_rate += (double)result.process(test_negatives_features_values, k);

		for (uint32_t k = 0; k < test_positives_features_values._w; k++)
			new_true_positive_rate += (double)result.process(test_positives_features_values, k);

		new_false_positive_rate 	/= (double)test_negatives_features_values._w;
		new_true_positive_rate 		/= (double)test_positives_features_values._w;

		LOG(std::cout << "\t\t\t\t\t-> FP: " << new_false_positive_rate << ", TP: " << new_true_positive_rate << std::endl)

		false_positive_rate = new_false_positive_rate;
		true_positive_rate = new_true_positive_rate;
	}

	return true;
}
