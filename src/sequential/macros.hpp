
#ifndef __MACROS_HPP__
#define __MACROS_HPP__


#define SAMPLE_WIDTH		24
#define SAMPLE_HEIGHT		24


#define CHECK_EXCEPTIONS		// Define this macro to add access control for matrix and images
#define TRAIN_SAMPLES_PER_TYPE	20  // Define those macros to set a limit for loaded samples
#define TEST_SAMPLES_PER_TYPE	10

//#define MAX_SAMPLES			3		  // Define this macro to set a limit for loaded samples


/* Macros testWeak */ 
#define NB_FEATURE 4120
#define NB_IMAGE 512  // Max 512 car multiplié par 2 

/* Macros adaboost */ 
#define ENABLE_FEATURE_TYPE_1
//#define ENABLE_FEATURE_TYPE_2
//#define ENABLE_FEATURE_TYPE_3
//#define ENABLE_FEATURE_TYPE_4
// #define ENABLE_FEATURE_TYPE_5

#define ALLOW_LOGS

#define BUCKETS_SIZE 		30 		// Number of negative samples we load at each step when searching false postive samples 
									// to construct the test set for a cascade classifier training step


#define ADABOOST_FALSE_POS_MAX_THRESHOLD 0.2
#define ADABOOST_TRUE_POS_MIN_THRESHOLD 0.9

// DEBUG PART

#ifndef ALLOW_LOGS
#define LOG(msg)
#else
#define LOG(msg) msg;
#endif

// Types definitions

#include <cstdint>

typedef uint16_t 	fparam_t;	// Types for features parameters (i, j, h, w)
typedef int32_t		fvalue_t;	// Types for features computation result, can be < 0
typedef uint32_t	iicoef_t;	// Types for integral images and images coefficients


#endif /* __MACROS_HPP__ */
