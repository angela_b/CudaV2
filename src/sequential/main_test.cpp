#include <iostream>
#include <vector>
#include <cstdint>
#include <ctime>
#include <cstdlib>

#include <opencv2/opencv.hpp>
#include <opencv2/imgcodecs.hpp>

#include "macros.hpp"

#include "classification/cascade_classifier.hpp"

void detect_face(cv::Mat& frame, CascadeClassifier& cc,std::vector<std::vector<fparam_t>>& features_parameters)
{

	uint32_t height = frame.rows;
	uint32_t width = frame.cols;
	uint32_t scale = 1;

	height = frame.rows / scale;
	width = frame.cols / scale;

	while (height >= SAMPLE_HEIGHT && width >= SAMPLE_WIDTH)
	{
		std::cout << height << ' ' << width << std::endl;
		cv::Mat rescaled;
		cv::resize(frame, rescaled, cv::Size(width, height));

		uint32_t ct = 0;
		uint32_t incr = 1;

		#pragma omp parallel for
		for (uint32_t i = 0; i <= height - SAMPLE_HEIGHT; i += incr)
		{
			for (uint32_t j = 0; j <= width - SAMPLE_WIDTH; j += incr)
			{
				ct += 1;
				cv::Rect bounds(j, i, SAMPLE_WIDTH, SAMPLE_HEIGHT);
				cv::Mat croppedImage = rescaled(bounds);
				cv::Mat gray_scaled;
				cv::cvtColor(croppedImage, gray_scaled, cv::COLOR_BGR2GRAY);

				Matrix<iicoef_t> sample(SAMPLE_HEIGHT, SAMPLE_WIDTH);

				for (int i1 = 0; i1 < gray_scaled.cols; i1++)
					for (int j1 = 0; j1 < gray_scaled.rows; j1++)
						sample.set((iicoef_t)gray_scaled.at<uchar>(j1, i1), i1, j1);
				
				Matrix<iicoef_t> integral_sample;
				get_integralimage(sample, integral_sample);
				Matrix<fvalue_t> features_values(features_parameters.size(), 1);
				compute_features_values(SAMPLE_HEIGHT, SAMPLE_WIDTH, &integral_sample, 0, features_values);


				auto res = cc.process(features_values);
				
				if (res == 1)
					cv::rectangle(frame, cv::Rect(j * scale, i * scale, SAMPLE_WIDTH * scale, SAMPLE_HEIGHT * scale), cv::Scalar(100, 0, 255));
			}
		}
		
		scale += 1;
		height = frame.rows / scale;
		width = frame.cols / scale;
	}
}


int main(int argc, char** argv) 
{
	if (argc != 3) 
	{
		std::cerr << "Usage: ./ViolaJonesSequential path/to/cascade_classifier_results.txt path/to/image.jpg" << std::endl;
		return 1;
	}


	std::string cascade_classifier_results_file_path(argv[1]);
	std::ifstream in(cascade_classifier_results_file_path);

	CascadeClassifier cascade_classifier;
	in >> cascade_classifier;

	// Computation of features parameters
	LOG(std::cerr << "[INFO] computing the features parameters for samples of dimensions: (" << SAMPLE_WIDTH << ", " << SAMPLE_HEIGHT << ")" << std::endl)
	std::vector<std::vector<fparam_t>> features_parameters;
	compute_features_parameters(SAMPLE_HEIGHT, SAMPLE_WIDTH, features_parameters);
	LOG(std::cerr << "[INFO] " << features_parameters.size() << " features parameters computed" << std::endl)

	std::string path(argv[2]);
	cv::Mat frame = cv::imread(path);

	detect_face(frame, cascade_classifier, features_parameters);


	// Plot the images with detection windows
	cv::namedWindow("Face detection", 1);
	for(;;)
	{
		cv::imshow("Face detection", frame);
		if (cv::waitKey(30) >= 0) break;
	}
	return 0;
}