#ifndef __MATRIX_HPP__
#define __MATRIX_HPP__


#include <cstdint>
#include <stdexcept>
#include <iostream>

#include "../macros.hpp"

// Matrix of size H x L
template<class T>
class Matrix {

public:
	Matrix();
	Matrix(uint32_t h, uint32_t w);
	Matrix(uint32_t h, uint32_t w, T default_value);
	~Matrix();

	void init(uint32_t h, uint32_t w);
	int size() { return _h * _w;}
	// Getters & setters 
	T at(uint32_t i, uint32_t j) const;
	void set(T value, uint32_t i, uint32_t j);

	void clear();

	// Operations
	void transpose();
	void get_transpose(Matrix<T>& m) const;
	void operator*=(T d);

	void dot(Matrix<T>& m2);
	void dot(Matrix<T>& m2, Matrix<T>& res) const;

	T sum() const;

	uint32_t _h 	= 0;
	uint32_t _w 	= 0;
	T* _array		= nullptr;


	// Static functions
	static void identity(uint32_t n, Matrix<T>& m);

};

template<typename T>
std::ostream& operator<<(std::ostream& os, const Matrix<T>& m);


#include "matrix.hxx"

#endif /* __MATRIX_HPP__ */