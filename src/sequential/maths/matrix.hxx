#ifndef __MATRIX_HXX__
#define __MATRIX_HXX__

#include "matrix.hpp"

template<typename T>
Matrix<T>::Matrix() {}

template<typename T>
Matrix<T>::Matrix(uint32_t h, uint32_t w)
	: _h(h)
	, _w(w)
{
	this->_array = new T[h * w];
}

template<typename T>
Matrix<T>::Matrix(uint32_t h, uint32_t w, T default_value)
	: _h(h)
	, _w(w)
{
	this->_array = new T[h * w];
	for (uint32_t i = 0; i < this->_h * this->_w; i++) 
		this->_array[i] = default_value;
}

template<typename T>
Matrix<T>::~Matrix() {
	delete[] this->_array;
}

template<typename T>
void Matrix<T>::init(uint32_t h, uint32_t w) {
	this->clear();
	this->_h = h;
	this->_w = w;
	this->_array = new T[h * w];
}

template<typename T>
T Matrix<T>::at(uint32_t i, uint32_t j) const {
	
	#ifdef CHECK_EXCEPTIONS
	if (i >= this->_h || j >= this->_w) throw std::out_of_range("Matrix<T>::at() : index out of range");
	#endif

	return this->_array[i * this->_w + j];
}

template<typename T>
void Matrix<T>::set(T value, uint32_t i, uint32_t j) {
	
	#ifdef CHECK_EXCEPTIONS
	if (i >= this->_h || j >= this->_w) throw std::out_of_range("Matrix<T>::set() : index out of range");
	#endif

	this->_array[i * this->_w + j] = value;
}

template<typename T>
void Matrix<T>::clear() {
	delete[] this->_array;
	this->_array = nullptr;
	this->_h = 0;
	this->_w = 0;
}

// Operations

template<typename T>
void Matrix<T>::transpose() {
	T *new_array = new T[this->_w * this->_h];
	
	for (uint32_t i = 0; i < this->_h; i++) {
		for (uint32_t j = 0; j < this->_w; j++) {
			new_array[j * this->_h + i] = this->_array[i * this->_w + j];
		}
	}

	uint32_t new_h = this->_w;
	this->_w = this->_h;
	this->_h = new_h;

	delete[] this->_array;
	this->_array = new_array;
}

template<typename T>
void Matrix<T>::get_transpose(Matrix<T>& m) const {
	T *new_array = new T[this->_w * this->_h];
	for (uint32_t i = 0; i < this->_h; i++) {
		for (uint32_t j = 0; j < this->_w; j++) {
			new_array[j * this->_h + i] = this->_array[i * this->_w + j];
		}
	}

	delete[] m._array;
	m._array = new_array;
	m._w = this->_h;
	m._h = this->_w;
}

template<typename T>
void Matrix<T>::operator*=(T d) {
	for (uint32_t i = 0; i < this->_w * this->_h; i++)
		this->_array[i] *= d;
}


template<typename T>
void Matrix<T>::dot(Matrix<T>& m2) {
	
	#ifdef CHECK_EXCEPTIONS
	if (this->_w != m2._h) throw std::out_of_range("Matrix<T>::dot(Matrix &m2) : invalid shapes");
	#endif

	T *new_array = new T[this->_h * m2._w];

	for (uint32_t i = 0; i < this->_h; i++) {
		for (uint32_t j = 0; j < m2._w; j++) {
			T sum = 0;
			for (uint32_t k = 0; k < this->_w; k++) {
				sum += this->at(i, k) * m2.at(k, j);
			}
			new_array[i * m2._w + j] = sum;
		}
	}

	delete[] this->_array;
	this->_array = new_array;
	this->_w = m2._w;

}

template<typename T>
void Matrix<T>::dot(Matrix<T>& m2, Matrix<T>& res) const {
	
	#ifdef CHECK_EXCEPTIONS
	if (this->_w != m2._h) throw std::out_of_range("Matrix<T>::dot(Matrix &m2) : invalid shapes");
	#endif

	T *new_array = new T[this->_h * m2._w];

	for (uint32_t i = 0; i < this->_h; i++) {
		for (uint32_t j = 0; j < m2._w; j++) {
			T sum = 0;
			for (uint32_t k = 0; k < this->_w; k++) {
				sum += this->at(i, k) * m2.at(k, j);
			}
			new_array[i * m2._w + j] = sum;
		}
	}

	delete[] res._array;
	res._array = new_array;
	res._w = m2._w;
	res._h = this->_h;
}

template<typename T>
T Matrix<T>::sum() const {
	T S = 0;
	for (uint32_t i = 0; i < this->_h; i++) {
		for (uint32_t j = 0; j < this->_w; j++) {
			S += this->at(i, j);
		}
	}
	return S;
}


// Static functions 

template<typename T>
void Matrix<T>::identity(uint32_t n, Matrix<T>& m) {
	m._h = n;
	m._w = n;

	delete[] m._array;
	m._array = new T[n * n];

	for (uint32_t i = 0; i < n; i++) {
		for (uint32_t j = 0; j < n; j++) {
			m.set((i == j ? 1 : 0), i, j);
		}
	}
}


template<typename T>
std::ostream& operator<<(std::ostream& os, const Matrix<T>& m) {

	os << '[';

	for (uint32_t i = 0; i < m._h; i++) {
		if (i > 0) os << ' ';
		os << '[';
		for (uint32_t j = 0; j < m._w; j++) {
			os << '\t' << m.at(i, j) << (j == m._w - 1 ? ']' : ',');
		}
		os << (i == m._h - 1 ? ']' : '\n');
	}

	return os;
}

#endif /* MATRIX_HXX_ */