#include "strong_classifier.hpp"


StrongClassifier::StrongClassifier() 
:	_coefficients()
,	_features_indexs()
,	_alphas_sum(0)
{}


void StrongClassifier::add_coefficients(double alpha, fvalue_t threshold, fvalue_t sign_threshold, uint32_t feature_index) 
{

	std::vector<double> new_coefficients(3);

	new_coefficients[0] = alpha;
	new_coefficients[1] = threshold;
	new_coefficients[2] = sign_threshold;

	this->_coefficients.push_back(new_coefficients);
	this->_features_indexs.push_back(feature_index);

	this->_alphas_sum += alpha;
}



// Return the strong classifier decision on an unique image
fvalue_t StrongClassifier::process(const Matrix<fvalue_t> &image_features_values) 
{
	return this->process(image_features_values, 0);
}


// Return the strong classifier decision on an image at index image_index
fvalue_t StrongClassifier::process(const Matrix<fvalue_t> &images_features_values, uint32_t image_index) 
{
	double sum = 0;

	for (uint32_t t = 0; t < this->_coefficients.size(); t++) 
		sum += this->_coefficients[t][0] * process_weak_classifier(this->_coefficients[t][1], this->_coefficients[t][2], images_features_values.at(this->_features_indexs[t], image_index));

	return sum >= 0.5 * this->_alphas_sum ? 1 : 0;
};


std::ostream& operator<<(std::ostream& os, const StrongClassifier& sc)
{
	os << sc._coefficients.size() << std::endl;

	for (uint32_t i = 0; i < sc._coefficients.size(); i++)
	{
		os 	<< 	sc._coefficients[i][0] << std::endl  
			<< 	sc._coefficients[i][1] << std::endl
			<< 	sc._coefficients[i][2] << std::endl
			<< 	sc._features_indexs[i];
		if (i < sc._coefficients.size() - 1)
			os << std::endl;
	}

	return os;
}

std::istream& operator>>(std::istream& is, StrongClassifier& sc)
{
	uint32_t coefficients_size;
	is >> coefficients_size;

	double alpha;
	fvalue_t threshold;
	fvalue_t sign_threshold;
	uint32_t feature_index;

	for (uint32_t i = 0; i < coefficients_size; i++)
	{
		is >> alpha;
		is >> threshold;
		is >> sign_threshold;
		is >> feature_index;
		sc.add_coefficients(alpha, threshold, sign_threshold, feature_index);
	}	

	return is;
}
