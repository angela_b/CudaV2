#ifndef __CASCADE_CLASSIFIER_HPP__
#define __CASCADE_CLASSIFIER_HPP__

#include <cstdint>
#include <vector>
#include <string>
#include <algorithm>

#include "../macros.hpp"
#include "../maths/matrix.hpp"
#include "strong_classifier.hpp"
#include "adaboost.hpp"
#include "../images/image.hpp"

/**

Algorithme train :

Initialement, on a un set de valeurs de features précalculées sur les datas positives
On définit un nombre de stage S
Pour chaque stage s < S:
	On génère un nombre N (> nombres d'images positives) de data négatives prises au hasard sur notre dataset de datas négatives
	On calcule pour chacune de ces données négatives la résultat des features
	On applique Adaboost :
		Condition d'arrêt : on calcule à chaque étape à l'aide d'un set de validation le taux de true positives et de false positives à l'aide d'un set de validation, et une fois qu'ils dépassent un certain seuil on break
	On enregistre le strong classifier généré par Adaboost
**/



class CascadeClassifier 
{
public:

	CascadeClassifier();
	~CascadeClassifier();


	void train(uint32_t num_stages, std::string positives_samples_urls_file_path, std::string negatives_samples_urls_file_path);
	
	fvalue_t process(const Matrix<fvalue_t>& image_features_values);
	fvalue_t process(const Matrix<fvalue_t>& images_features_values, uint32_t image_index);

	std::vector<StrongClassifier *> _strong_classifiers;
};

std::ostream& operator<<(std::ostream& os, const CascadeClassifier& cc);
std::istream& operator>>(std::istream& is, CascadeClassifier& cc);



#endif /* __CASCADE_CLASSIFIER_HPP__ */