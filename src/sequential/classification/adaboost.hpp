#ifndef __ADABOOST_HPP__
#define __ADABOOST_HPP__

#include <math.h>
#include <stdlib.h>
#include <vector>
#include <cstdint>

#include "../macros.hpp"
#include "../maths/matrix.hpp"
#include "strong_classifier.hpp"
#include "weak_classifier.hpp"

bool adaboost(	StrongClassifier &result, uint32_t max_periodes, 
				const Matrix<fvalue_t>& train_positives_features_values, const Matrix<fvalue_t>& train_negatives_features_values,
				const Matrix<fvalue_t>& test_positives_features_values, const Matrix<fvalue_t>& test_negatives_features_values,
				const std::vector<std::vector<fparam_t>>& features_parameters);

#endif /* __ADABOOST_HPP__ */