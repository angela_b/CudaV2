#ifndef __STRONG_CLASSIFIER_HPP__
#define __STRONG_CLASSIFIER_HPP__

#include <cstdint>
#include <vector>
#include <iostream>

#include "../macros.hpp"
#include "../maths/matrix.hpp"
#include "weak_classifier.hpp"

class StrongClassifier 
{
public:
	
	StrongClassifier();

	void add_coefficients(double alpha, fvalue_t threshold, fvalue_t sign_threshold, uint32_t feature_index);

	// Return the strong classifier decision on an unique image
	fvalue_t process(const Matrix<fvalue_t> &image_features_values);
	// Return the strong classifier decision on an image at index image_index
	fvalue_t process(const Matrix<fvalue_t> &images_features_values, uint32_t image_index);


	std::vector<std::vector<double>> _coefficients;
	std::vector<uint32_t> _features_indexs;
	double _alphas_sum;
};


std::ostream& operator<<(std::ostream& os, const StrongClassifier& sc);
std::istream& operator>>(std::istream& is, StrongClassifier& sc);


#endif /* __STRONG_CLASSIFIER_HPP__ */