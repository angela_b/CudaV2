#include "weak_classifier.hpp"


// Given a threshold and a sign associed to this threshold, return the estimated class for a value
fvalue_t process_weak_classifier(fvalue_t threshold, fvalue_t sign, fvalue_t value)
{
	return sign * value >= sign * threshold  ? 1 : 0;
}


// Find the best threshold between positive and negative samples features values
void train_weak_classifier(	uint32_t feature_index, const Matrix<fvalue_t> &positives_features_values, const Matrix<fvalue_t> &negatives_features_values, 
							fvalue_t &best_threshold, fvalue_t &sign_best_threshold	) 
{
	// get the number of samples
	uint32_t num_positives_samples = positives_features_values._w;
	uint32_t num_negatives_samples = negatives_features_values._w;
	uint32_t num_samples = num_positives_samples + num_negatives_samples;
	best_threshold = 0;
	sign_best_threshold = 1;

	uint32_t min_error = num_samples;
	fvalue_t threshold;
	bool best_is_pos ;
	fvalue_t signs[] = {-1, 1};	
	bool is_positive = true;
	for (uint32_t j = 0; j < num_samples; j++) 
	{
		// Init the value at index j as a threshold
		threshold = j < num_positives_samples ? 
			positives_features_values.at(feature_index, j) : negatives_features_values.at(feature_index, j - num_positives_samples);
		is_positive = j < num_positives_samples;
		for (fvalue_t sign : signs) 
		{
			// Compute the errors got by this threshold
			uint32_t error = 0;

			for (uint32_t j2 = 0; j2 < num_positives_samples; j2++) 
			{
				fvalue_t res = process_weak_classifier(threshold, sign, positives_features_values.at(feature_index, j2));
				// In this loop we are checking for positives samples, labeled 1
				if (res != 1) { error += 1; }
			}

			for (uint32_t j2 = 0; j2 < num_negatives_samples; j2++) 
			{
				fvalue_t res = process_weak_classifier(threshold, sign, negatives_features_values.at(feature_index, j2));
				// In this loop we are checking for negatives samples, labeled -1
				if (res != 0) { error += 1; }
			}
			// If the error is the min error, then the threshold associed to the sign may be the one which minimized the error
			if (error < min_error) 
			{
				best_is_pos = is_positive;
				best_threshold = threshold;
				sign_best_threshold = sign;
				min_error = error;

			}
		}
	}
//	std::cout << "Feature " << feature_index << " is positive ? " <<best_is_pos << "With error " << min_error << std::endl;
	//if (feature_index < 10)
	//	std:: cout << "For feature " << feature_index << " best error "<< min_error << std::endl;
}