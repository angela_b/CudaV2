#ifndef __WEAK_CLASSIFIER_HPP__
#define __WEAK_CLASSIFIER_HPP__

#include <cstdint>

#include "../maths/matrix.hpp"

// Given a threshold and a sign associed to this threshold, return the estimated class for a value
fvalue_t process_weak_classifier(fvalue_t threshold, fvalue_t sign, fvalue_t value);

// Find the best threshold between positive and negative samples features values
void train_weak_classifier(	uint32_t feature_index, const Matrix<fvalue_t> &positives_features_values, const Matrix<fvalue_t> &negatives_features_values, 
							fvalue_t &best_threshold, fvalue_t &sign_best_threshold	);


#endif /* __WEAK_CLASSIFIER_HPP__ */