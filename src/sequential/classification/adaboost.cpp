#include "adaboost.hpp"


bool adaboost(	StrongClassifier &result, uint32_t max_periodes, 
				const Matrix<fvalue_t>& train_positives_features_values, const Matrix<fvalue_t>& train_negatives_features_values,
				const Matrix<fvalue_t>& test_positives_features_values, const Matrix<fvalue_t>& test_negatives_features_values,
				const std::vector<std::vector<fparam_t>>& features_parameters) 
{
	uint32_t N = train_positives_features_values._w + train_negatives_features_values._w;
	uint32_t num_features = train_positives_features_values._h;

	Matrix<fvalue_t> weak_classifiers = Matrix<fvalue_t>(num_features, 2);

	LOG(std::cerr << "\t\t-> computing strong classifier" << std::endl)

	// Computation of weak classifiers
	LOG(std::cerr << "\t\t\t-> training weak classifiers" << std::endl)


	#ifdef PARALLEL
	LOG(std::cerr << '\r' << "\t\t\t\t-> weak_classifiers training" << std::endl)
	#pragma omp parallel for
	#endif
	for (uint32_t i = 0; i < num_features; i++) 
	{
		fvalue_t threshold;
		fvalue_t sign;
		train_weak_classifier(i, train_positives_features_values, train_negatives_features_values, threshold, sign);
		weak_classifiers.set(threshold, i, 0);
		weak_classifiers.set(sign, i, 1);
		#ifndef PARALLEL
		LOG(std::cerr << '\r' << "\t\t\t\t-> " << i + 1 << "/" << num_features << " weak_classifiers trained" << std::flush)
		#endif

	}
	#ifndef PARALLEL
	LOG(std::cerr << std::endl);
	#endif
	
	LOG(std::cerr << "\t\t\t-> initializing weights" << std::endl)
	
	Matrix<double> weights = Matrix<double>(N, 1);

	double positives_initial_weights = 1 / (2 * (double)train_positives_features_values._w);
	double negatives_initial_weights = 1 / (2 * (double)train_negatives_features_values._w);

	for (uint32_t i = 0; i < train_positives_features_values._w; i++) 
		weights.set(positives_initial_weights, i, 0);

	for (uint32_t i = train_negatives_features_values._w; i < N; i++)
		weights.set(negatives_initial_weights, i, 0);

	double false_positive_rate 	= 1;
	double true_positive_rate 	= 0;
	uint32_t current_period = 0;
	while (current_period < max_periodes && (false_positive_rate > ADABOOST_FALSE_POS_MAX_THRESHOLD || true_positive_rate < ADABOOST_TRUE_POS_MIN_THRESHOLD))
	{
		current_period += 1;
		LOG(std::cerr << "\t\t\t-> period: " << current_period << std::endl)

		// Normalize the weights
		LOG(std::cerr << "\t\t\t\t-> normalize the weights" << std::endl)
		weights *= 1 / weights.sum();

		double min_error = 1;
		uint32_t best_weak_classifier_index = 0;

		for (uint32_t i = 0; i < num_features; i++) {
			LOG(std::cerr << '\r' << "\t\t\t\t-> evaluating error of weak classifier " << i + 1 << "/" << num_features << std::flush)
			
			double error = 0;
			for (uint32_t j1 = 0; j1 < train_positives_features_values._w; j1++) 
			{
				error += weights.at(j1, 0) * abs((double)process_weak_classifier(weak_classifiers.at(i, 0), weak_classifiers.at(i, 1), train_positives_features_values.at(i, j1)) - 1);
			}

			Matrix<double> error_neg(train_negatives_features_values._w, 1);
			for (uint32_t j2 = 0; j2 < train_negatives_features_values._w; j2++) 
			{
				error += weights.at(train_positives_features_values._w + j2, 0) * abs((double)process_weak_classifier(weak_classifiers.at(i, 0), weak_classifiers.at(i, 1), train_negatives_features_values.at(i, j2)) - 0);
			}

			if (error < min_error) 
			{
				min_error = error;
				best_weak_classifier_index = i;
			}
		}

		LOG(std::cerr << std::endl)

		LOG(std::cerr 	<< "\t\t\t\t-> best weak classifier found: " << best_weak_classifier_index 
						<< " (type: " 	<< features_parameters[best_weak_classifier_index][0] 
						<< ", i: " 		<< features_parameters[best_weak_classifier_index][1]
						<< ", j; " 		<< features_parameters[best_weak_classifier_index][2]
						<< ", h: " 		<< features_parameters[best_weak_classifier_index][3]
						<< ", w: " 		<< features_parameters[best_weak_classifier_index][4] << ")"
						<< ", error: " << min_error << std::endl)

		for (uint32_t f = 0; f < result._features_indexs.size(); f++) {
			if (result._features_indexs[f] == best_weak_classifier_index)
			{
				LOG(std::cerr << "\t\t\t\t-> same feature found as previous period" << std::endl)
				LOG(std::cerr << "\t\t\t-> stopping strong classifier computation" << std::endl)
				return false;
			}
		}

		LOG(std::cerr << "\t\t\t\t-> updating the weights" << std::endl)
		double beta 	= min_error / (1 - min_error);

		if (abs(0 - beta) < 1e-4)
		{
			LOG(std::cerr << "\t\t\t\t-> enable to find another discriminant feature, not enough data" << std::endl)
			LOG(std::cerr << "\t\t\t-> stopping strong classifier computation" << std::endl)
			return false;
		}


		double alpha 	= log(1 / beta);

		for (uint32_t j1 = 0; j1 < train_positives_features_values._w; j1++) 
		{
			double w_classifier_res = process_weak_classifier(	weak_classifiers.at(best_weak_classifier_index, 0), 
																weak_classifiers.at(best_weak_classifier_index, 1), 
																train_positives_features_values.at(best_weak_classifier_index, j1));
			double e = abs(w_classifier_res - 1);

			weights.set( weights.at(j1, 0) * exp((1 - e) * log(beta)), j1, 0);
		}

		for (uint32_t j2 = 0; j2 < train_negatives_features_values._w; j2++) 
		{
			double w_classifier_res = process_weak_classifier(	weak_classifiers.at(best_weak_classifier_index, 0), 
																weak_classifiers.at(best_weak_classifier_index, 1), 
																train_negatives_features_values.at(best_weak_classifier_index, j2));
			double e = abs(w_classifier_res - 0);

			weights.set( weights.at(train_negatives_features_values._w + j2, 0) * exp((1 - e) * log(beta)), train_negatives_features_values._w + j2, 0);
		}

		result.add_coefficients(alpha, weak_classifiers.at(best_weak_classifier_index, 0), weak_classifiers.at(best_weak_classifier_index, 1), best_weak_classifier_index);

		// Computation of true positive and false positive rate
		LOG(std::cerr << "\t\t\t\t-> evaluating true positive and false positive rate using test sets" << std::endl)
		double new_false_positive_rate = 0;
		double new_true_positive_rate = 0;
		for (uint32_t k = 0; k < test_negatives_features_values._w; k++)
			new_false_positive_rate += (double)result.process(test_negatives_features_values, k);

		for (uint32_t k = 0; k < test_positives_features_values._w; k++)
			new_true_positive_rate += (double)result.process(test_positives_features_values, k);

		new_false_positive_rate 	/= (double)test_negatives_features_values._w;
		new_true_positive_rate 		/= (double)test_positives_features_values._w;

		LOG(std::cout << "\t\t\t\t\t-> FP: " << new_false_positive_rate << ", TP: " << new_true_positive_rate << std::endl)



		false_positive_rate = new_false_positive_rate;
		true_positive_rate = new_true_positive_rate;
	}

	return true;
}
