#include "cascade_classifier.hpp"

CascadeClassifier::CascadeClassifier()
: _strong_classifiers()
{}

CascadeClassifier::~CascadeClassifier()
{
	for (StrongClassifier * sc : this->_strong_classifiers)
		delete sc;
}


fvalue_t CascadeClassifier::process(const Matrix<fvalue_t>& image_features_values) 
{
	return this->process(image_features_values, 0);
}

fvalue_t CascadeClassifier::process(const Matrix<fvalue_t>& images_features_values, uint32_t image_index) 
{

	for (StrongClassifier* sc : this->_strong_classifiers)
		if (sc->process(images_features_values, image_index) == 0)
			return 0;

	return 1;
}

void CascadeClassifier::train(uint32_t num_stages, std::string positives_samples_urls_file_path, std::string negatives_samples_urls_file_path) 
{
	// Computation of features parameters
	LOG(std::cerr << "[INFO] computing the features parameters for samples of dimensions: (" << SAMPLE_WIDTH << ", " << SAMPLE_HEIGHT << ")" << std::endl)
	std::vector<std::vector<fparam_t>> features_parameters;
	compute_features_parameters(SAMPLE_HEIGHT, SAMPLE_WIDTH, features_parameters);
	LOG(std::cerr << "[INFO] " << features_parameters.size() << " features parameters computed" << std::endl)

	// Loading positive samples
	LOG(std::cerr << "[INFO] loading positive samples from: " << positives_samples_urls_file_path << std::endl)
	std::vector<Matrix<iicoef_t> *> positive_samples;
	readimages(positives_samples_urls_file_path, positive_samples, TRAIN_SAMPLES_PER_TYPE + TEST_SAMPLES_PER_TYPE);
	LOG(std::cerr << "[INFO] " << positive_samples.size() << " sample(s) loaded." << std::endl)

	// Loading train and test sets for positives samples
	std::vector<Matrix<iicoef_t> *> train_positive_samples(positive_samples.begin(), positive_samples.begin() + TRAIN_SAMPLES_PER_TYPE);
	std::vector<Matrix<iicoef_t> *> test_positive_samples(positive_samples.begin() + TRAIN_SAMPLES_PER_TYPE, positive_samples.end());
	LOG(std::cerr << "[INFO] " << "created train positive dataset with " << train_positive_samples.size() << " samples" << std::endl);
	LOG(std::cerr << "[INFO] " << "created test positive dataset with " << test_positive_samples.size() << " samples" << std::endl);

	positive_samples.clear();

	uint32_t n_train_positive_samples = train_positive_samples.size();
	uint32_t n_test_positive_samples = test_positive_samples.size();

	// Computation of integral images
	LOG(std::cerr << "[INFO] computing integral images..." << std::endl)
	Matrix<iicoef_t> *train_integral_positive_samples = new Matrix<iicoef_t>[n_train_positive_samples];
	Matrix<iicoef_t> *test_integral_positive_samples = new Matrix<iicoef_t>[n_test_positive_samples];

	for (uint32_t i = 0; i < n_train_positive_samples; i++) 
	{ 
		LOG(std::cerr << '\r' << '\t' << "-> " << "positive train set: integral images " << i + 1 << "/" << n_train_positive_samples << " computed" << std::flush)
		get_integralimage(*train_positive_samples[i], train_integral_positive_samples[i]);

	}
	LOG(std::cerr << std::endl)
	for (uint32_t i = 0; i < n_test_positive_samples; i++) 
	{ 
		LOG(std::cerr << '\r' << '\t' << "-> " << "positive test set: integral images " << i + 1 << "/" << n_test_positive_samples << " computed" << std::flush)
		get_integralimage(*test_positive_samples[i], test_integral_positive_samples[i]);

	}
	LOG(std::cerr << std::endl)

	LOG(std::cerr << "[INFO] freeing resources used by the positive samples" << std::endl)
	for (Matrix<iicoef_t>* sample : train_positive_samples) { delete sample; }
	for (Matrix<iicoef_t>* sample : test_positive_samples) { delete sample; }
	train_positive_samples.clear();
	test_positive_samples.clear();

	// Computation of features values for positive samples
	Matrix<fvalue_t> train_positive_features_values(features_parameters.size(), n_train_positive_samples);
	Matrix<fvalue_t> test_positive_features_values(features_parameters.size(), n_test_positive_samples);

	LOG(std::cerr << "[INFO] " << "applying the features on positive samples..." << std::endl)
	for (uint32_t i = 0; i < n_train_positive_samples; i++)
	{
		compute_features_values(SAMPLE_HEIGHT, SAMPLE_WIDTH, train_integral_positive_samples, i, train_positive_features_values);
		LOG(std::cerr << '\r' << '\t' << "positive train set: computed features values for " << (i + 1) << '/' << n_train_positive_samples << " samples" << std::flush)
	}
	LOG(std::cerr << std::endl)
	for (uint32_t i = 0; i < n_test_positive_samples; i++)
	{
		compute_features_values(SAMPLE_HEIGHT, SAMPLE_WIDTH, test_integral_positive_samples, i, test_positive_features_values);
		LOG(std::cerr << '\r' << '\t' << "positive test set: computed features values for " << (i + 1) << '/' << n_test_positive_samples << " samples" << std::flush)

	}
	LOG(std::cerr << std::endl)

	// Raw positive samples are not used anymore
	LOG(std::cerr << "[INFO] freeing resources used by the positive integral images" << std::endl)
	delete[] train_integral_positive_samples;
	delete[] test_integral_positive_samples;
	LOG(std::cerr << "Pepito " << std::endl)
	// Loading negative samples urls
	std::vector<std::string> negative_samples_urls;
	geturls(negatives_samples_urls_file_path, negative_samples_urls);
	LOG(std::cerr << "[INFO] " << negative_samples_urls.size() << " urls for negative samples loaded" << std::endl);

	// Training cascade classifier
	LOG(std::cerr << "[INFO] training cascade classifier" << std::endl)
	for (uint32_t current_stage = 1; current_stage <= num_stages; current_stage++)
	{
		LOG(std::cerr << "\t" << "-> stage " << current_stage << "/" << num_stages << std::endl)

		// Construction of negative samples dataset for this stage
		// All negative samples must be accepted as false positive
		
		// Shuffle negatives urls
		std::random_shuffle(negative_samples_urls.begin(), negative_samples_urls.end());
		// Load urls buckets of fixed size (time consumption increase but memory usage decrease)

		Matrix<fvalue_t> negative_features_values(features_parameters.size(), TRAIN_SAMPLES_PER_TYPE + TEST_SAMPLES_PER_TYPE);
		uint32_t bucket_size = BUCKETS_SIZE;
		uint32_t false_positives_count = 0;
		uint32_t i = 0;

		LOG(std::cerr << '\r' << "\t\t" << "-> found " << false_positives_count << " false positive sample" << std::flush)
		while (i < negative_samples_urls.size()) 
		{
			std::vector<Matrix<iicoef_t> *> bucket_negative_samples;
			readimages(negative_samples_urls, bucket_negative_samples, i, (i + bucket_size > negative_samples_urls.size() ? negative_samples_urls.size() : i + bucket_size));
			uint32_t n_bucket_negative_samples = bucket_negative_samples.size();
			
			Matrix<iicoef_t> *bucket_integral_negative_samples = new Matrix<iicoef_t>[n_bucket_negative_samples];
			for (uint32_t k = 0; k < n_bucket_negative_samples; k++)
				get_integralimage(*bucket_negative_samples[k], bucket_integral_negative_samples[k]);
			
			Matrix<fvalue_t> bucket_negative_features_values(features_parameters.size(), n_bucket_negative_samples);
			for (uint32_t k = 0; k < n_bucket_negative_samples; k++)
				compute_features_values(SAMPLE_HEIGHT, SAMPLE_WIDTH, bucket_integral_negative_samples, k, bucket_negative_features_values);

			for (Matrix<iicoef_t>* sample : bucket_negative_samples) { delete sample; }
			delete[] bucket_integral_negative_samples;
			
			for (uint32_t k = 0; k < n_bucket_negative_samples; k++)
			{
				double processed_value = this->process(bucket_negative_features_values, k);
				if (processed_value == 1) // got a false positive sample
				{
					// Copy the computed features values for the sample
					for (uint32_t f = 0; f < features_parameters.size(); f++)
						negative_features_values.set(bucket_negative_features_values.at(f, k), f, false_positives_count);

					false_positives_count += 1;

					LOG(std::cerr << '\r' << "\t\t" << "-> found " << false_positives_count << " false positive samples" << std::flush)

					if (false_positives_count >= TRAIN_SAMPLES_PER_TYPE + TEST_SAMPLES_PER_TYPE)
						break;
				}
			}

			i += bucket_size;
			if (false_positives_count >= TRAIN_SAMPLES_PER_TYPE + TEST_SAMPLES_PER_TYPE)
				break;
		}
		LOG(std::cerr << std::endl)

		uint32_t n_train_negative_samples 	= (TRAIN_SAMPLES_PER_TYPE * false_positives_count) / (TRAIN_SAMPLES_PER_TYPE + TEST_SAMPLES_PER_TYPE);
		uint32_t n_test_negative_samples 	= (TEST_SAMPLES_PER_TYPE * false_positives_count) / (TRAIN_SAMPLES_PER_TYPE + TEST_SAMPLES_PER_TYPE);
		Matrix<fvalue_t> train_negative_features_values(features_parameters.size(), n_train_negative_samples);
		Matrix<fvalue_t> test_negative_features_values(features_parameters.size(), n_test_negative_samples);
		
		for (uint32_t f = 0; f < features_parameters.size(); f++)
		{
			for (uint32_t i = 0; i < n_train_negative_samples; i++)
				train_negative_features_values.set(negative_features_values.at(f, i), f, i);
			for (uint32_t i = 0; i < n_test_negative_samples; i++)
				test_negative_features_values.set(negative_features_values.at(f, i + n_train_negative_samples), f, i);
		}

		negative_features_values.clear();

		LOG(std::cerr << "\t\t" << "-> negative train set: computed features values for " << n_train_negative_samples << " samples" << std::endl)
		LOG(std::cerr << "\t\t" << "-> negative test set: computed features values for " << n_test_negative_samples << " samples" << std::endl)

		// Once sets are computed, let's call adaboost algorithm to get a strong classifier

		StrongClassifier* strong_classifier = new StrongClassifier();
		bool system_change = adaboost(	*strong_classifier, features_parameters.size() / 10,  					// Set the max number of features selection to 10% of the total number of features
										train_positive_features_values, train_negative_features_values, 
										test_positive_features_values, test_negative_features_values,
										features_parameters);
		this->_strong_classifiers.push_back(strong_classifier);

		if (!system_change) { break; }
	}

	LOG(std::cerr << "[INFO]" << " cascade classifier training complete" << std::endl)
}

std::ostream& operator<<(std::ostream& os, const CascadeClassifier& cc)
{
	os << cc._strong_classifiers.size() << std::endl;
	
	for (uint32_t i = 0; i < cc._strong_classifiers.size(); i++)
	{
		os << *cc._strong_classifiers[i];
		if (i < cc._strong_classifiers.size() - 1)
			os << std::endl;
	}

	return os;
}

std::istream& operator>>(std::istream& is, CascadeClassifier& cc)
{
	uint32_t strong_classifiers_num;
	is >> strong_classifiers_num;

	for (uint32_t i = 0; i < strong_classifiers_num; i++)
	{
		StrongClassifier* sc = new StrongClassifier();
		is >> *sc;
		cc._strong_classifiers.push_back(sc);
	}

	return is;
}

