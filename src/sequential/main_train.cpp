#include <iostream>
#include <vector>
#include <cstdint>
#include <ctime>
#include <cstdlib>

#include "macros.hpp"

#include "classification/cascade_classifier.hpp"

int main(int argc, char** argv) 
{
	if (argc != 4) 
	{
		std::cerr << "Usage: ./ViolaJonesSequential path/to/positives_samples_urls.txt path/to/negatives_samples_urls.txt path/to/cascade_classifier_results.txt" << std::endl;
		return 1;
	}

	// Initialize radoms seed 
	std::srand(unsigned (std::time(0)));

	std::string positives_samples_urls_file_path(argv[1]);
	std::string negatives_samples_urls_file_path(argv[2]);

	CascadeClassifier cascade_classifier;
	cascade_classifier.train(2, positives_samples_urls_file_path, negatives_samples_urls_file_path);

	std::string cascade_classifier_results_file_path(argv[3]);
	std::ofstream out(cascade_classifier_results_file_path);

	out << cascade_classifier << std::endl;

	return 0;
}
