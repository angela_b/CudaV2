# Implémentation de l'algorithme de Viola Jones pour la reconnaissance de visages sur une image


## Partie séquentielle

<<<<<<< HEAD
Pour executer la partie séquentielle, il faut effectuer les étapes suivantes :

* Dans la racine du projet, lancer la commande :

```
> python3 ./scripts/to_raw_image.py
```

pour convertir les images dans un format brut.
Ce script génère les images positives et négatives dans les dossiers suivants :

```
resources/raw_negatives_samples/
resources/raw_positives_samples/
```

Ainsi que deux fichiers repertoriant les chemins de toutes les images en format brut générées :

```
resources/raw_negatives_samples.txt
resources/raw_positives_samples.txt
```

* Pour lancer le binaire, il suffit ensuite d'effectuer les commandes suivantes, toujours à la racine :
=======
Cette partie entraîne un classifier Cascade selon l'algorithme de ViolaJones.

* Pour lancer le binaire, il suffit ensuite d'effectuer les commandes suivantes, à la racine :
>>>>>>> Update README

```
> cmake .
> make ViolaJonesSequential
> python3 ./scripts/
> ./ViolaJonesSequential ./resources/positives_samples.txt ./resources/negatives_samples.txt path/to/classifier_result.txt
```

Il est possible de modifier certains paramètres dans le fichier :

```
src/sequential/macros.hpp
```

## Pour la partie Cuda
<<<<<<< HEAD

* Les tests sont unitaires uniquement, ils permettent de tester les weaks et leurs temps d'éxécutions, le reste n'étant  plus fonctionnels ou abandonné (comme le calcul parallèle d'image intégrale) 

Pour lancer les tests de vitesse sur le weak classifier, a la racine du projet : 
```
$ python3 scripts/to_raw_images.py
$ make -f src/Makefile TestWeak
$ TestParallelWeak pour tester la generation des weak classifier avec openMP parallel for
$ TestWeak pour tester la génération des weak classifier en Cuda et en sequentiel

```

=======

* Les tests sont unitaires uniquement, ils permettent de tester les weaks et leurs temps d'éxécutions, le reste n'étant  plus fonctionnels ou abandonné (comme le calcul parallèle d'image intégrale) 
>>>>>>> Update README

Pour lancer les tests de vitesse sur le weak classifier, a la racine du projet : 
```
$ python3 scripts/to_raw_images.py 
$ make -f src/Makefile TestWeak
$ TestParallelWeak pour tester la generation des weak classifier avec openMP parallel for
$ TestWeak pour tester la génération des weak classifier en Cuda et en sequentiel

```
La partie python est fondamentale, elle permet de generer les samples que va utiliser le classifieur.
Pour tester les différentes valeurs et vitesses il faut changer les macros dans src/macros.hpp.
Attention a ne pas depasser le nombre d'image 512 et le nombre de feature 65532
sans quoi le code n'est plus fonctionnel en raison de la mauvaise implementation 


Pour tester la version inclue dans ababoost : 


Pour lancer les tests de vitesse sur le weak classifier, a la racine du projet : 
```
<<<<<<< HEAD
$ make -f src/Makefile TestAda
$ TestAda ./resources/to_raw_positives_samples.txt ./resources/to_raw_negative_samples.txt fichier.txt
=======
$ make -f src/Makefile TestWeak
$ TestParallelWeak pour tester la generation des weak classifier avec openMP parallel for
$ TestWeak pour tester la génération des weak classifier en Cuda et en sequentiel

>>>>>>> Update README
```
Pour tester les différentes valeurs et vitesses il faut changer les macros dans src/macros.hpp.
Attention a ne pas depasser le nombre d'image 512 et le nombre de feature 65532
sans quoi le code n'est plus fonctionnel en raison de la mauvaise implementation 


Pour tester la version inclue dans ababoost : 


<<<<<<< HEAD
=======
Pour lancer les tests de vitesse sur le weak classifier, a la racine du projet : 
```
$ make -f src/Makefile TestAda
$ TestAda ./resources/to_raw_positives_samples.txt ./resources/to_raw_negative_samples.txt fichier.txt
```
>>>>>>> Update README
