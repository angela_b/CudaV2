import os
from functools import reduce
from PIL import Image, ImageOps


# Images in source folder must be grayscaled

POSITIVES_IMAGES_SOURCE_PATH		=	"./resources/positives_samples"
NEGATIVES_IMAGES_SOURCE_PATH		=	"./resources/negatives_samples"

RAW_IMAGES_SIZE			=	(24, 24)

IMAGES_EXTENSIONS		= [".jpg", ".jpeg", ".png"]

IMAGES_LIMIT			=  None


def load_files_absolutes_paths(path, get_absolute_path=False):
	files_paths = []
	for root, dirs, files in os.walk(path):
		for file in files:
			valid_file = False
			for extension in IMAGES_EXTENSIONS:
				if extension in file:
					valid_file = True
					break
			if valid_file:
				files_paths.append(os.path.abspath(root + '/' + file) if get_absolute_path else file)
	return files_paths


def list_files(IMAGES_SOURCE_PATH):
	
	images_names	=	load_files_absolutes_paths(IMAGES_SOURCE_PATH)
	if (not(IMAGES_LIMIT is None) and IMAGES_LIMIT < len(images_names)):
		images_names = images_names[:IMAGES_LIMIT]

	# Generate a file recensing all raw images urls
	raw_images_names	=	load_files_absolutes_paths(IMAGES_SOURCE_PATH, get_absolute_path=True)
	urls_file_path		=	reduce(lambda a, b: a + b, [i + '/' for i in IMAGES_SOURCE_PATH.split('/')[:-1]]) + IMAGES_SOURCE_PATH.split('/')[-1] + ".txt"

	urls_file 			= 	open(urls_file_path, 'w')
	urls_file.write(str(len(raw_images_names)) + '\n')
	for url in raw_images_names:
		urls_file.write(url + '\n')

	urls_file.close()

	print(urls_file_path)

list_files(POSITIVES_IMAGES_SOURCE_PATH)
list_files(NEGATIVES_IMAGES_SOURCE_PATH)

