import numpy as np
import matplotlib.pyplot as plt
import matplotlib

def plot_feature(t, I, J, H, W):
    
    image = np.ones((24, 24)) * 125
    
    if t == 1:
        for i in range(I, I + H):
            for j in range(J, J + W):
                image[i, j] = 0
            for j in range(J + W, J + 2 * W):
                image[i, j] = 255
    elif t == 3:
        for j in range(J, J + W):
            for i in range(I, I + H):
                image[i, j] = 0
            for i in range(I + H, I + 2 * H):
                image[i, j] = 255     
    elif t == 2:
        for i in range(I, I + H):
            for j in range(J, J + W):
                image[i, j] = 0
            for j in range(J + W, J + 2 * W):
                image[i, j] = 255
            for j in range(J + 2 * W, J + 3 * W):
                image[i, j] = 0
    
    elif t == 4:
        for j in range(J, J + W):
            for i in range(I, I + H):
                image[i, j] = 0
            for i in range(I + H, I + 2 * H):
                image[i, j] = 255  
            for i in range(I + 2 * H, I + 3 * H):
                image[i, j] = 0

    elif t == 5:
        for i in range(I, I + H):
            for j in range(J, J + W):
                image[i, j] = 0
            for j in range(J + W, J + 2 * W):
                image[i, j] = 255
        for i in range(I + H, I + 2 * H):
            for j in range(J, J + W):
                image[i, j] = 255
            for j in range(J + W, J + 2 * W):
                image[i, j] = 0
                
    plt.imshow(image, cmap='Greys_r')
    plt.show()


plot_feature(2, 8, 0, 6, 4)