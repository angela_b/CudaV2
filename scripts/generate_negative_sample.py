from PIL import Image, ImageOps
import os

IMAGES_EXTENSIONS		= [".jpeg", ".jpg", ".png"]

CROP_SIZE 		=	(24, 24)
IMAGES_FOLDER 	= 	"/Users/florian/Informatique/Cours/ING3/Programmation GPU/Projet/images/"
DEST 			= 	"/Users/florian/Informatique/Cours/ING3/Programmation GPU/Projet/ViolaJonesCUDA/resources/negatives_samples/"
ID 				=	0

def load_files_absolutes_paths(path, get_absolute_path=False):
	files_paths = []
	for root, dirs, files in os.walk(path):
		for file in files:
			valid_file = False
			for extension in IMAGES_EXTENSIONS:
				if extension in file:
					valid_file = True
					break
			if valid_file:
				files_paths.append(os.path.abspath(root + file) if get_absolute_path else file)
	return files_paths

images_src = load_files_absolutes_paths(IMAGES_FOLDER, get_absolute_path=True)

for image_src in images_src:
	im = Image.open(image_src)
	im.thumbnail((500, 500), Image.ANTIALIAS)
	print(im.size)


	samples_name = DEST + image_src.split('/')[-1].split('.')[0] + "_sample_"

	width, height = im.size
	square = max(width, height)

	while square > CROP_SIZE[1] and square > CROP_SIZE[0]:

		im.thumbnail((square, square), Image.ANTIALIAS)
		width, height = im.size

		for i in range(0, height - CROP_SIZE[0], 20):
			for j in range(0, width - CROP_SIZE[1], 20):
				print(ID)
				sample = im.crop((j, i, j + CROP_SIZE[0], i + CROP_SIZE[1]))
				sample.thumbnail(CROP_SIZE, Image.ANTIALIAS)
				sample.convert('L')
				ImageOps.grayscale(sample).save(samples_name + str(ID) + '.jpg')
				sample.close()
				ID += 1

		square -= 50

	im.close()

